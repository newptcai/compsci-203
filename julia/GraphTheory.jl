module GraphTheory

using Graphs, Multigraphs, GraphPlot, Cairo, Compose, Colors, Random, GraphRecipes, Plots, PyCall

export get_eulerian,
    get_bipartite,
    is_euerlian,
    save_graph,
    save_graph_recipe,
    get_knoigsberg,
    get_graph,
    is_even_degree,
    is_odd_degree,
    clique_num,
    has_degree

function get_eulerian(n, m)
    return get_graph(n, m, is_euerlian)
end

function get_bipartite(n1, n2, m)
    g = SimpleGraph(n1+n2)

    e = ne(g)

    while e < m && e <  n1*n2
        v1 = rand(1:n1)
        v2 = rand(n1+1:n1+n2)
        add_edge!(g, v1, v2)
        e = ne(g)
    end

    return g
end

function get_graph(n, m, cond, seed=1234)
    Random.seed!(seed)

    g = erdos_renyi(n, m)
    while ! cond(g)
        g = erdos_renyi(n, m)
    end

    return g
end

# This is not very useful since we cannot draw multi-graphs.
function get_knoigsberg()
    g = Multigraph(4)
    for i in 2:4
        add_edge!(g, 1, i)
    end

    add_edge!(g, 2, 3, 2)
    add_edge!(g, 3, 4, 2)

    return g
end

function is_euerlian(g)
    if !is_connected(g) || !is_even_degree(g)
        return false
    end
    return true
end

function is_even_degree(g)
    return all(map(iseven, degree(g)))
end

function is_odd_degree(g)
    return all(map(isodd, degree(g)))
end

function has_degree(g, d)
    return any(map(x->x==d, degree(g)))
end

function save_graph(g, alphabet=true, C=20; kwargs...)
    g_path = tempname() * ".pdf"
    if alphabet
        nodelabel = ('a':'z')[1:nv(g)]
    else
        nodelabel = 1:nv(g)
    end
    layout=(args...)->spring_layout(args...; C=20)
    draw(PDF(g_path, 8cm, 8cm), 
        gplot(g, 
            layout=layout,
            nodesize=1,
            nodelabeldist=2, 
            nodelabelangleoffset=π/4,
            nodelabel=nodelabel,
            nodestrokec=colorant"black",
            nodestrokelw=2,
            nodefillc=colorant"orange";
            kwargs...))
    return g_path
end

function save_graph(g, x_loc::Vector{T}, y_loc::Vector{T}, w=8, h=8; kwargs...) where T <: Number
    g_path = tempname() * ".pdf"
    draw(PDF(g_path, (w)cm, (h)cm), 
        gplot(g, x_loc, y_loc, 
            nodesize=1,
            nodelabeldist=2, 
            nodelabelangleoffset=π/4,
            nodestrokec=colorant"black",
            nodestrokelw=2,
            nodefillc=colorant"orange";
            kwargs...))
    return g_path
end

function get_petersen()
    g = SimpleGraph(10)

    for i in 1:5
        add_edge!(g, i, mod(i+1, 5))
        add_edge!(g, 5 + i,  5 + mod(i+1, 5))
        add_edge!(g, i, i+5)
    end

    return g
end


function save_graph_recipe(g;kw...)
    pyplot()
    g_path = tempname() * ".pdf"
    plt = graphplot(g; 
                    method = :stress,
                    fontsize = 10,
                    names=1:nv(g),
                    curves=false,
                    nodeshape=:circle, 
                    nodesize=0.2,
                    linecolor=:black,
                    markercolor=:orange,
                    self_edge_size=0.25,
                    kw...)
    savefig(plt, g_path)
    return g_path
end

function clique_num(g)
    cliques = maximal_cliques(g)
    
    return length(cliques[1])
end

function graph_with_5_clique()
    g = get_graph(8, 17, g->clique_num(g) == 5, 1236)
    return save_graph(g, false, 100)
end

function graph_g3()
    g = cycle_graph(5)
    nodelabel = ["x($i)" for i in 1:nv(g)]
    nodelabeldist= fill(2, nv(g))
    nodelabeldist[1]=-2
    nodelabeldist[5]=-2
    return save_graph(g, false, 100;
                      layout=circular_layout, 
                      nodelabel=nodelabel,
                      nodelabeldist=nodelabeldist
                     )
end

function make_g4()
    n = 5
    g = cycle_graph(n)

    # duplicate vertices

    add_vertices!(g, n)

    for e in edges(g)
        if e.src <= n && e.dst <= n
            add_edge!(g, e.src, n + e.dst)
            add_edge!(g, n + e.src, e.dst)
        end
    end

    add_vertex!(g)

    z = 2*n + 1
    for i in n + 1:2*n
        add_edge!(g, z, i)
    end

    return g, n, z
end

function make_g4(gsmall)
    n = nv(gsmall)
    g = copy(gsmall)

    # duplicate vertices

    add_vertices!(g, n+1)

    for e in edges(g)
        if e.src <= n && e.dst <= n
            add_edge!(g, e.src, n + e.dst)
            add_edge!(g, n + e.src, e.dst)
        end
    end

    z = 2*n + 1
    for i in n + 1:2*n
        add_edge!(g, z, i)
    end

    return g, n, z
end

function graph_g4_1()
    g, n, z = make_g4()

    loc1 = hcat([[cos(2*π/n*i), sin(2*π/n*i)] for i in 1:n]...)
    loc2 = 2 * loc1
    loc = hcat(loc1, loc2, [0.6; -0.2])

    return save_g4(g, loc, n, z)
end

function graph_g4_2()
    g, n, z = make_g4()

    loc1 = hcat([[cos(2*π/n*i), sin(2*π/n*i)] for i in 1:n]...)[:, [1, 4, 2, 5, 3]]
    loc2 = 2*hcat([[cos(2*π/n*i + π/n), sin(2*π/n*i + π/n)] for i in 1:n]...)[:, [3, 1, 4, 2, 5]]
    loc = hcat(loc1, loc2, [0; 0])

    return save_g4(g, loc, n, z)
end

function graph_g4_3()
    g, n, z = make_g4()

    loc1 = hcat([[3*i, 0] for i in 1:n]...)
    loc2 = hcat([[3*i, -1] for i in 1:n]...)    
    loc = hcat(loc1, loc2, [3 * (1+5)/2; -2])
    nodelabeldist= fill(3, nv(g))

    return save_g4(g, loc, n, z; linetype="curve", nodelabeldist=nodelabeldist,outangle = -π / 20 )
end

function graph_g4_4()
    g, n, z = make_g4()

    cl = [colorant"purple", colorant"blue", colorant"red", colorant"green"]
    nodefillc=vcat(cl[[1,2,1,2,3]], fill(cl[4], n), cl[1])

    loc1 = hcat([[3*i, 0] for i in 1:n]...)
    loc2 = hcat([[3*i, -1] for i in 1:n]...)    
    loc = hcat(loc1, loc2, [3 * (1+5)/2; -2])
    nodelabeldist= fill(3, nv(g))

    return save_g4(g, loc, n, z;
                   linetype="curve",
                   nodelabeldist=nodelabeldist,
                   outangle = -π / 20,
                   nodefillc=nodefillc,)
end

function graph_g4_5()
    g, n, z = make_g4()

    cl = [colorant"purple", colorant"blue", colorant"red"]
    nodefillc=vcat(cl[[1,2,1,2,3]], fill(colorant"white", n+1))

    loc1 = hcat([[3*i, 0] for i in 1:n]...)
    loc2 = hcat([[3*i, -1] for i in 1:n]...)    
    loc = hcat(loc1, loc2, [3 * (1+n)/2; -2])
    nodelabeldist= fill(3, nv(g))

    return save_g4(g, loc, n, z;
                   linetype="curve",
                   nodelabeldist=nodelabeldist,
                   outangle = -π / 20,
                   nodefillc=nodefillc,)
end

function graph_g5()
    g, n, z = make_g4()
    g, n, z = make_g4(g)

    loc1 = hcat([[i, 0] for i in 1:n]...)
    loc2 = hcat([[i, -1] for i in 1:n]...)    
    loc = hcat(loc1, loc2, [(1+n)/2; -2])
    nodelabeldist= fill(3, nv(g))

    return save_g4(g, loc, n, z;
                   NODESIZE=0.03,
                   linetype="curve",
                   nodelabeldist=nodelabeldist,
                   outangle = -π / 8,
                   nodelabel=nothing,
                  )
end

function save_g4(g, loc, n, z; kwargs...)
    el = edges(g) |> collect
    edgestrokec = fill(colorant"lightblue", ne(g))
    for i in 1:ne(g)
        e = el[i]
        if e.src <= n && e.dst <= n
            edgestrokec[i] = colorant"lightgray"
        end
        if e.src == z || e.dst == z
            edgestrokec[i] = colorant"pink"
        end
    end

    nodelabel = vcat(["x($i)" for i in 1:n], ["y($i)" for i in 1:n], "z")
    nodelabeldist= fill(3, nv(g))
    nodelabeldist[n + 1]=-3
    nodelabeldist[n + n]=-3
    return save_graph(g, loc[1, :], loc[2, :]; 
                      NODESIZE=0.05,
                      nodestrokelw=0.2,
                      nodelabel=nodelabel,
                      nodelabeldist=nodelabeldist,
                      edgestrokec=edgestrokec,
                      kwargs...
                     )
end

function graph_g4_kelly()
    n = 5
    g = cycle_graph(5)
    ni = (n-1)* 3 + 1
    add_vertices!(g, 2*n+ni)

    for e in edges(g)
        if e.src <= n && e.dst <=n
            add_edge!(g, e.src + n, e.dst +n)
            add_edge!(g, e.src + 2*n, e.dst + 2*n)
        end
    end

    for i in 1:5
        add_edge!(g, i, i + 3 *n)
    end

    loc1 = hcat([[1 + cos(2*π/n*i), sin(2*π/n*i)] for i in 1:n]...)
    loc2 = hcat([[6 + cos(2*π/n*i), sin(2*π/n*i)] for i in 1:n]...)    
    loc3 = hcat([[12 + cos(2*π/n*i), sin(2*π/n*i)] for i in 1:n]...)    
    loc4 = hcat([[i, -2] for i in 1:ni]...)
    loc = hcat(loc1, loc2, loc3, loc4)

    cl = [colorant"purple", colorant"blue", colorant"yellow", colorant"blue", colorant"green"]
    nodefillc = vcat(repeat(cl, outer=3), fill(colorant"white", ni))
    nodestrokec=vcat(fill(colorant"black", 3*n), fill(colorant"red", ni)) 

    g_path = tempname() * ".pdf"
    draw(PDF(g_path, 24cm, 6cm), 
         gplot(g, 
               loc[1, :],
               loc[2, :]; 
               NODESIZE=0.03,
               nodestrokelw=0.2,
               nodefillc=nodefillc,
               nodestrokec=nodestrokec,
              ))
    return g_path
end

# Make a random bipartite graph
function save_bipartite()
    g = get_bipartite(3, 4, 7)
    return save_graph(g;
                      nodelabel=nothing,
                     )
end

# Make a random graph
function save_random()
    Random.seed!(1234)
    g = erdos_renyi(7, 12)
    nodefillc = fill(colorant"orange", nv(g))
    nodefillc[2] = colorant"green"
    nodefillc[4] = colorant"blue"
    nodefillc[5] = colorant"red"
    nodefillc[7] = colorant"blue"
    return save_graph(g;
                      nodefillc = nodefillc
                     )
end

# Make a cycle graph
function save_cycle(n)
    g = cycle_graph(n)
    return save_graph(g; layout=circular_layout)
end

# Make a complete graph
function save_complete(n)
    g = complete_graph(n)
    return save_graph(g; layout=circular_layout)
end

# Make a graph for Brook's theorem
function make_brook(n)
    g = complete_graph(n-1)
    add_vertex!(g)
    add_edge!(g, 2, n)
    return g
end

# Save a graph for Brook's theorem
function save_brook_line()
    n = 5
    g = make_brook(n)
    x_loc = collect(1:n)
    y_loc = fill(0, n)
    nodelabel=["v($i)" for i in 1:n]

    return save_graph(g, x_loc, y_loc, 24, 12;
                      nodelabeldist=4, 
                      outangle=-π/2,
                      NODESIZE=0.05,
                      NODELABELSIZE=8,
                      nodelabel=nodelabel, 
                      linetype="curve")
end

# Make a graph for Brook's theorem
function save_brook_natural()
    n = 5
    g = make_brook(n)

    nodelabel=["v($i)" for i in 1:n]
    nodelabeldist=fill(2.5, n)
    nodelabeldist[[1, 3]] .*= -1

    Random.seed!(1234)
    return save_graph(g;
                      nodelabelangleoffset=π/2,
                      nodelabeldist=nodelabeldist, 
                      NODESIZE=0.1,
                      NODELABELSIZE=6,
                      nodelabel=nodelabel)
end

# Just to make all graphs similar
function save_bowtie()
    n = 5
    g = SimpleGraph(5)
    eset = [1 2;
             2 3;
             3 1;
             1 4;
             4 5;
             5 1;]
    for e in eachrow(eset)
        add_edge!(g, e[1], e[2])
    end

    x_loc = [0; -1; -1; 1; 1]
    y_loc = [0; 1; -1; 1; -1]

    nodelabel=["v($i)" for i in 1:n]
    nodelabeldist=fill(4, n)
    nodelabeldist[[3, 5]] .*= -1

    return save_graph(g, x_loc, y_loc, 16, 8;
                      nodelabeldist=nodelabeldist,
                      nodelabelangleoffset=π/2,
                      NODESIZE=0.1,
                      NODELABELSIZE=6,
                      nodelabel=nodelabel)
end

function save_complete(n)
    g = complete_graph(n)

    return save_graph(g, false; layout=circular_layout)
end

function save_hole(n)
    g = cycle_graph(n)

    return save_graph(g;
                      nodelabel=nothing,
                      layout=circular_layout)
end

function save_anti_hole(n)
    g = Graphs.complement(cycle_graph(n))

    return save_graph(g;
                      nodelabel=nothing,
                      layout=circular_layout)
end

# Make a graph for the utility example
function save_uitlity()
    g = complete_bipartite_graph(3, 3)
    x_loc = 4 * vcat(fill(0, 3), fill(1, 3))
    y_loc = repeat(1:3, 2)
    labels_1 = ["Water", "Electricity", "Natural Gas"]
    labels_2 = "Home " .* string.(1:3)
    nodelabel = vcat(labels_1, labels_2)
    nodelabeldist = fill(-5, 6)
    nodelabeldist[4:6] .*= -1
    return save_graph(g, x_loc, y_loc, 16, 8;
                      nodelabelangleoffset=π,
                      nodelabeldist=nodelabeldist, 
                      nodelabel=nodelabel,
                      NODELABELSIZE=8,
                     )
end

function save_bipartite()
    g = complete_bipartite_graph(3, 3)
    x_loc = 4 * vcat(fill(0, 3), fill(1, 3))
    y_loc = repeat(1:3, 2)
    nodelabeldist = fill(-5, 6)
    nodelabeldist[4:6] .*= -1
    return save_graph(g, x_loc, y_loc, 8, 8;
                      nodelabelangleoffset=π,
                      nodelabeldist=nodelabeldist, 
                      nodelabel=nothing,
                     )
end

function save_k4_planar()
    g = complete_graph(4)
    loc = hcat([[cos(π/2+i*2*π/3), -sin(π/2+i*2*π/3)] for i in 0:2]...)
    loc = hcat(loc, [0, 0])
    x_loc = loc[1, :]
    y_loc = loc[2, :]
    return save_graph(g, x_loc, y_loc;
                      nodelabel=nothing,
                     )
end

# An example of number of faces
function save_planar()
    n = 6
    g = SimpleGraph(n)
    eset = [1 4;
            1 2;
            1 5;
            1 6;
            2 3;
            2 5;
            3 4;
            3 5;
            4 6;
           ]

    for e in eachrow(eset)
        add_edge!(g, e...)
    end

    Random.seed!(1233)
    x_loc, y_loc = spring_layout(g)

    x_loc[2] += 1
    y_loc[2] += 0.5

    return save_graph(g, x_loc, y_loc;
                      nodelabel=nothing,
                     )
end

# An example of face edge pairs
function save_face_edge()
    n = 5
    g = SimpleGraph(n)
    eset = [1 4;
            1 2;
            1 5;
            2 3;
            2 5;
            3 5;
           ]

    for e in eachrow(eset)
        add_edge!(g, e...)
    end

    Random.seed!(1234)
    x_loc, y_loc = spring_layout(g)
    loc = hcat(x_loc, y_loc)
    loc[2, :] .+= 0.3

    return save_graph(g, loc[:, 1], loc[:, 2];
                      nodelabel=nothing,
                     )
end

function save_i6()
    g = SimpleGraph(6)
    return save_graph(g;
                      layout=circular_layout,
                      nodelabel=nothing,
                     )
end


function save_wedding_graph(seed=1234;kwargs...)
    g = SimpleGraph(9)

    Random.seed!(seed)

    for i in 1:5
        for j in 6:9
            if true == rand(0:1)
                add_edge!(g, i, j)
            end
        end
    end

    names = pyimport("names")
    fnames = [names.get_first_name(gender="female") for i in 1:5]
    mnames = [names.get_first_name(gender="male") for i in 6:9]

    nodelabel = vcat(fnames, mnames)
    nodefillc = vcat(
                     fill(colorant"orange", 5),
                     fill(colorant"turquoise", 4))
    x_loc = 2 * vcat(1:5, 1.5:1:4.5)
    y_loc = vcat(fill(1.0, 5), fill(2.0, 4))

    g_path = tempname() * ".pdf"
    draw(PDF(g_path, 16cm, 8cm), 
        gplot(g, 
            x_loc,
            y_loc,
            nodesize=1,
            nodelabeldist=2.2, 
            nodelabelangleoffset=π/4,
            nodelabel=nodelabel,
            nodestrokec=colorant"black",
            nodestrokelw=2,
            nodefillc=nodefillc;
            kwargs...))
    return g_path
end


## Save the graph
function save_clique_number()
    Random.seed!(1235)
    for i in 1:3
        n = 10
        p = 0.4
        g = Graphs.SimpleGraphs.erdos_renyi(n, p)
        cliques = maximal_cliques(g)
        cliquenum = maximum(length.(cliques))
        println("Clique number is $cliquenum")
        nodelabel = 1:n
        plt = gplot(g, nodelabel=nodelabel, NODESIZE=0.1, NODELABELSIZE=9)
        draw(PNG("/tmp/clique-number-01-$i.png", 16cm, 16cm, dpi=300), plt)
    end
end

# Create a disconnected graph
function save_disconnected_eulerian()
    g = CaiGraphs.get_graph(6, 6, x->(CaiGraphs.is_even_degree(x) 
                                      && !CaiGraphs.has_degree(x, 0)
                                      && !is_connected(x)), 1234)
    gp = CaiGraphs.save_graph(g, false)
    run(`xdg-open $gp`)
    return gp
end

# Graph with isolated vertex
function save_isolated_eulerian()
    g = CaiGraphs.get_graph(6, 6, x->(CaiGraphs.is_even_degree(x) 
                                    && CaiGraphs.has_degree(x, 0)
                                    && !is_connected(x)), 1234)
    gp = CaiGraphs.save_graph(g, false)
    run(`xdg-open $gp`)
    return gp
end

function save_eulerian(m=6, n=6)
    g = CaiGraphs.get_eulerian(m, n)   
    gp = CaiGraphs.save_graph(g, false)
    run(`xdg-open $gp`)
    return gp
end

function save_hamiltonian(n=7)
    low = convert(Int, ceil(n/2))
    m = convert(Int, ceil(n*low/2))
    if isodd(m)
        m = m+1
    end
    cond = g -> minimum(degree(g)) >= low
    g = CaiGraphs.get_graph(n, m, cond)
    gp = CaiGraphs.save_graph(g, false)
    run(`xdg-open $gp`)
    return gp
end

## Create a simple graph
function save_k5()
    g = CompleteGraph(5)

    ## Set node labels
    nodelabel = 'a':'e'

    ## Layout list
    layoutlist = [spring_layout, random_layout, random_layout, circular_layout, spectral_layout, random_layout]

    ## Save the graph
    for i in 1:6
        plt = gplot(g, nodelabel=nodelabel, layout=layoutlist[i], NODESIZE=0.2, NODELABELSIZE=12)
        draw(PNG("/tmp/graph-k5-$i.png", 16cm, 16cm, dpi=300), plt)
    end
end

## Create a simple graph
function save_graph_example()
    g = SimpleGraph(5);
    edgelist = [(1,2), (3,4), (1,4)]
    for e in edgelist
        add_edge!(g, e...)
    end

    ## Set node labels
    nodelabel = 'a':'e'

    ## Save the graph
    for i in 1:6
        layout=(args...)->spring_layout(args...; C=15+2*i)
        plt = gplot(g, nodelabel=nodelabel, layout=layout, NODESIZE=0.2, NODELABELSIZE=12)
        draw(PNG("/tmp/graph-01-$i.png", 16cm, 16cm, dpi=300), plt)
    end
end

end

