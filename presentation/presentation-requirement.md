---
title: Presentation for COMPSCI 203
author: Xing Shi Cai
date: 2022-09-16
geometry:
- margin=1.5in
...

As part of the evaluation process,
you are required to give a 15-minutes presentation,
on a *problem* (or *problems*) in discrete mathematics.

Here are video recording of some [excellent talks](https://duke.box.com/s/lousc9o14adugoin2yu1jgjc2kye8wk6) 
given by previous students.

# The Topic

You can choose a problem of your like, as long as 

1. it belongs to logic, combinatorics, graph theory, or probability theory;
2. we have **not** covered the topic at the time of your presentation;
3. it is suitable for an audience of other students of this course.

Some books from which you may find a suitable problem ---

1. [Discrete Mathematics --- An Open Introduction, 3rd edition](https://discrete.openmathbooks.org/) (our textbook)
2. [Applied Combinatorics](https://www.rellek.net/book/app-comb.html) (our textbook)
3. [Concrete Mathematics: A Foundation for Computer Science](https://www-cs-faculty.stanford.edu/~knuth/gkp.html)
4. [Combinatorics Through Guided Discovery](https://bogart.openmathbooks.org/ctgd/ctgd.html)
5. [Graph Theory with Applications by Bondy and Murty](http://www.maths.lse.ac.uk/Personal/jozef/LTCC/Graph_Theory_Bondy_Murty.pdf)

You can also choose other sources such as a blog, a paper, a Youtube video, etc.

# The Format

You can use slides, write on a (electronic) white board, or combine different
presentation methods and technologies.

# The Schedule

The presentation schedule will be announced in a separate post on [Ed](https://edstem.org).

# Tips

1. Typeset math formulas using either a [formula editor](https://support.microsoft.com/en-us/office/equation-editor-6eac7d71-3c74-437b-80d3-c7dea24fdf3f),
   or LaTeX. Do not use screenshots from a book. 
   It looks very unprofessional.
2. Prepare a little bit more than 15-minutes' content. You can always cut it short
   when you are out of time.
3. Do at least one rehearse to see how much time it takes.

\pagebreak{}

# Rubrics

The grades which you will get depend on which of the following category
best fit your performance.

## Excellent (5pt)

1. The topic is suitable for the audience.
2. Have a set of well-prepared slides or notes. No typos. Professionally written math formulas.
3. Speak English fluently. 
4. Explain the topic clearly. 
5. Have a good motivation for the problem/application being presented. 
6. Have a sense of humour.
7. Finish the talk in about 15 minutes.
8. Well-prepared for questions from the audience.
9. Ask other students relevant questions about their presentations during a
   presentation session.

## Good (4pt)

1. The topic is suitable, but may be too difficult.
2. Have a set of well-prepared slides or notes. But there are a few typos. Math
   formulas are not professionally written.
3. Speak English mostly fluently. 
4. Explain the topic more or less clearly. 
6. Finish the talk in more than 15 minutes.
7. Can respond to questions from the audience in a reasonable way.
8. Ask other students questions about their presentations during a presentation
   meeting.

## Fine (3pt)

1. The topic is suitable, but too easy.
2. Have a set of reasonably-prepared slides or notes. Many typos.
3. Can explain the topic in English. 
4. Finish the talk in much less than 15 minutes.
5. Can respond to questions from the audience.

## Pass (2pt)

1. The topic is not very suitable.
2. Have a set of slides or notes.
3. Have some trouble explaining the topic in English. 
4. Finish the talk in much less than 10 minutes.
5. Have trouble respond to questions from the audience.

## Complete (1pt)

1. The presentation has been given.

## Incomplete (0pt)

1. The presentation has not been given.
