---
title: COMPSCI 203 Discrete Mathematics for Computer Science
subtitle: Midterm
author: "Instructor Xing Shi Cai"
date: 2022-09-15
fontsize: 12pt
numbersections: true
weave_options:
    echo: false
    results: "tex"
---

```julia

using Latexify
using LaTeXStrings
using Plots
using Random
using Distributions
using DataFrames
using SymPy

myseed = 1234;

function short_answer()
print(L"""
\smallskip{}
Answer: $\underline{\hspace{5cm}}$
""")
end;

function short_answer(ans)
print(L"""
\smallskip{}
Answer: $\underline{%$ans}$
""")
end;

Random.seed!(myseed);
```

Full Name: $\underline{\hspace{5cm}}$ NetID: $\underline{\hspace{5cm}}$

\begin{tcolorbox}[title={\centering{} \Large{} Honour Pledge}]
\large{}

$\text{\emoji{panda}}$
I will neither give nor receive aid on this assessment except the textbooks,
solutions manuals for the assignments.

\smallskip{}
Signature: $\underline{\hspace{5cm}}$
\end{tcolorbox}

# Random Pictures (0.1 pt)

Which picture appeared in this week's announcement?

* [ ] cake
* [X] pizza
* [ ] ice cream
* [ ] dumpling
* [ ] noodle

\pagebreak{}

# Logic (1pt)

## Implications

Which of the following statements are equivalent to the *converse* of the implication,
"if you :sleeping: well, then you will be :smile:."?

* [ ] Either you :sleeping: well or else you are :smile: ($P \vee Q$).
* [ ] You will :sleeping: well and be :smile: ($P \wedge Q$).
* [ ] If you are :smile:, you must have won the lottery (:zany_face:).
* [ ] You will be :smile: if you :sleeping: well ($P \implies Q$).
* [X] You will be :smile: only if you :sleeping: well ($Q \implies P$).
* [X] It is necessary for you to :sleeping: well to be :smile: ($Q \implies P$).
* [ ] If you are not :smile:, then you did not :sleeping: well ($\neg Q \implies \neg P$).
* [ ] You will :sleeping: well if and only if you are :smile: ($P \iff Q$).
* [X] Unless you :sleeping: well, you won't be :smile: ($\neg P \implies \neg Q$).
* [X] You will :sleeping: well if you are :smile: ($Q \implies P$).
* [ ] It is sufficient to :sleeping: well to be :smile: ($P \implies Q$).
* [X] Either you :sleeping: well or else you are not :smile: ($P \vee \neg Q$).

Remark:

Let $P$ be :sleeping: well and $Q$ be :smile:.
The converse of $P \implies Q$ is $Q \implies P$.

## Implications

Determine if the following is a valid deduction rule using a truth-table:
\begin{equation*}
    \begin{array}{ r l }
        & (P ∧ Q) \to R \\
        & ¬P ∨ ¬Q \\
        \cline{2-2}
        \therefore & R
    \end{array}
\end{equation*}

Solution:

\begin{equation*}
\left(
\begin{array}{ccccc}
 \text{P} & \text{Q} & \text{R} & \text{(P$\wedge $Q)$\Rightarrow $R} & \text{$\neg $P$\vee \neg $Q} \\
 \text{T} & \text{T} & \text{T} & \text{T} & \text{F} \\
 \text{T} & \text{T} & \text{F} & \text{F} & \text{F} \\
 \text{T} & \text{F} & \text{T} & \text{T} & \text{T} \\
 \text{T} & \text{F} & \text{F} & \text{T} & \text{T} \\
 \text{F} & \text{T} & \text{T} & \text{T} & \text{T} \\
 \text{F} & \text{T} & \text{F} & \text{T} & \text{T} \\
 \text{F} & \text{F} & \text{T} & \text{T} & \text{T} \\
 \text{F} & \text{F} & \text{F} & \text{T} & \text{T} \\
\end{array}
\right)
\end{equation*}

The fourth, sixth, and eighth rows show that the argument is *invalid*.

\pagebreak{}

# Integer Composition (1pt)

## Inclusion-exclusion

How many integer solutions are there to the inequality
\begin{equation*}
y_1 + y_2 + y_3 + y_4 < 155
\end{equation*}
with $y_1, y_2 > 0, 0 ≤ y_3 ≤ 8$, and $0 ≤ y_4 < 11$?
Find a solution using the \emph{inclusion-exclusion} principle.

:bulb: The answer should be written in terms of binomial coefficients.

Solution:

```julia
ans = binomial(155 + 2 -1,5 - 1) - 
    binomial(155 + 2 -9 -1,5 - 1) - 
    binomial(155 + 2 -11 -1,5 - 1) + 
    binomial(155 + 2 -9 -11 -1,5 - 1);
```

This is equivalent to finding the number of integer solutions of
\begin{equation}
y_1 + y_2 + y_3 + y_4 + y_5 = 155
\label{eq:y:format}
\end{equation}
with $y_1, y_2, y_5 > 0, 0 ≤ y_3 ≤ 8$, $0 ≤ y_4 < 11$,
or alternatively
\begin{equation}
    x_1 + x_2 + x_3 + x_4 + x_5 = 155 + 2
    \label{eq:x:format}
\end{equation}
with $x_1, x_2, x_5 > 0, 0 < x_3 ≤ 9$, and $0 < x_4 \le 11$.
So the answer is
\begin{equation*}
    \begin{aligned}
        &
        \binom{155 + 2 -1}{5 - 1}
        - 
        \binom{155 + 2 -9 -1}{5 - 1}
        -
        \binom{155 + 2 -11 -1}{5 - 1}
        +
        \binom{155 + 2 -9 -11 -1}{5 - 1}
        \\
        &
        =
        `j ans`
    \end{aligned}
\end{equation*}

## Generating Function

Write down a generating function which can be used to solve the above problem.

Solution:

The generating function for \eqref{eq:y:format} is

```julia

y = Sym("y")
gfstr = "(y/(1-y))^3*((1-y^9)/(1-y))*((1-y^11)/(1-y))"
gf = eval(Meta.parse(gfstr))
print(latexify(gf, env=:eq))
ans = series(gf, y, 0, 158).coeff(y^155);
```
The coefficient of $y^{155}$ in this GF is $`j ans`$.
The same as above.

Alternatively, we can use the generating function for \eqref{eq:x:format}, which is

```julia

x = Sym("x")
gf = (x/(1-x))^3*((x-x^10)/(1-x))*((x-x^12)/(1-x))
gfstr = latexify(gf, env=:eq)
print(gfstr)
ans = series(gf, x, 0, 158).coeff(x^157);
```
The coefficient of $x^{157}$ in this GF is $`j ans`$.
The same as above.


\pagebreak{}

# Generating Functions (1pt)

Let $a_n$ be the number of strings of length $n$
formed from the set $\{$ :apple:, :banana: $\}$ 
if there must be at least one :apple: and the number of :banana: must be odd. 

## Exponential Generating Function (EGF)

Write down the EGF of $(a_n)_{n \ge 0}$.

Answer:

```julia
x = Sym("x")
gf = (exp(x) - 1) * ((exp(x) - exp(-x))/2)
gfstr = latexify(gf, env=:eq)
print(gfstr);
```

## Exponential Generating Function (EGF)

Find a closed formula for $a_n$ from your EGF.
Explain how you get your formula.

Solution:

Expanding the generating function, we get

```julia
print(latexify(gf.expand(), env=:eq))
```

Thus, $a_0 = 0$ and
\begin{equation}
    a_n = 
    \frac{1}{2} 2^n
    - \frac{1}{2}
    + \frac{1}{2} (-1)^n
    \qquad
    (n \ge 1)
    .
\end{equation}

\pagebreak{}

# Breaking a :pizza: (1pt)

You want to break a :pizza: to pieces.
Each time you are only allowed to break one piece of :pizza:
into to two pieces.
Let $b_n$ be the number of breaks you need to get $n$ pieces of :pizza:.
Use *strong induction* to show that $b_n = n-1$ for all $n \ge 1$.

Proof:

The base case is when $n = 1$. Since we don't need to break the pizza to get one
piece of it, we have $b_1 = 0 = 1-1$.

Now assume that $b_k = k-1$ for all $1 \le k \le n$ where $n$ is a positive integer.

To get $n+1$ pieces, we need to make at least one break, which gives us two pieces.
Let's call them pizza $a$ and pizza $b$.
To get $n+1$ pieces, we need to break $a$ into $k$ pieces and $b$ into $n+1-k$
pieces for some $k \in \{1,\dots, n\}$.
So by the induction hypothesis, in total we need
\begin{equation*}
    1 + b_k + b_{n-k} = 1 + (k-1) + (n+1-k-1) = n
\end{equation*}
breaks.

\pagebreak{}

# Chess (1pt)

Consider a chess board of $n \times n$ grids.
Let $a_0 = 1$ and for $n \ge 1$ let $a_n$ be the number of ways to put $n$ rooks
on the board (each one in a different grid) such that

1. no two rooks are in the same column or row,
1. and no rook is on the diagonal (bottom-left to top-right).

![One possible placing of $5$ rooks on a $5 \times 5$ board](rook.png){width=200px}

## Formula

Find a formula to compute $a_n$.

:bulb: We have already seen this number in week 2.

```julia
short_answer("\\sum_{k=0}^n (-1)^k \\binom{n}{k} (n-k)!")
```
Remark:

Let $\sigma_i$ be the column number in which the rook in row $i$ is in.
Then $\sigma$ is permuation of $\{1, \dots, n\}$.
If no rook is allowed to be on the diagonal,
then $\sigma$ is a derangment.

\pagebreak{}

## An Identity

Show that $a_n$ satisfies
\begin{equation*}
    n! = 
    \binom{n}{0} a_n + 
    \binom{n}{1} a_{n-1} + 
    \binom{n}{2} a_{n-2} + 
    \dots
    \binom{n}{n} a_{0}
\end{equation*}
using a combinatorial argument.

Answer:

In a permutation $\sigma$ if $\sigma_i = i$, then $i$ is called a fixed point.

The LHS counts the number of all permutations of $\{1, \dots, n\}$.
The RHS counts the same number by grouping permutations according the number of fixed
points in them.
The number of permuations with $i$ fixed points is
$\binom{n}{i} a_{n-i}$, 
where $\binom{n}{i}$ is the number of ways to choose the positions of the fixed
points, and $a_{n-i}$ is the number of ways to arrage the other $n-i$ numbers in the
permuation so that they do not create fixed points.
