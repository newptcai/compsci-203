---
title: COMPSCI 203 Discrete Mathematics for Computer Science
subtitle: Quiz 01
author: "Instructor Xing Shi Cai"
date: 2022-09-01
fontsize: 12pt
numbersections: true
weave_options:
    echo: false
    results: "tex"
---

```julia

using Latexify
using LaTeXStrings
using Plots
using Random
using Distributions
using DataFrames

myseed = 1234;

function short_answer()
print(L"""
\smallskip{}
Answer: $\underline{\hspace{5cm}}$
""")
end;

```

Full Name: $\underline{\hspace{5cm}}$ NetID: $\underline{\hspace{5cm}}$

\begin{tcolorbox}[title={\centering{} \Large{} Honour Pledge}]
\large{}

$\text{\emoji{panda}}$
I will neither give nor receive aid on this assessment except the textbooks,
solutions manuals for the assignments and of course Julia.

\smallskip{}
Signature: $\underline{\hspace{5cm}}$
\end{tcolorbox}

# Random Pictures (0.1 pt)

Which animal's picture appeared in this week's announcement?

```julia
short_answer();
```

# Sets (1 pt)

Find the following cardinalities

## (a) {-}

```julia

Random.seed!(myseed);
low = rand(1:10);
up = low + 2 * rand(10:20);

```

$|A|$ when
\begin{equation*}
    A = \{`j low`, `j low+2`, `j low+4`, \ldots, `j up` \}.
\end{equation*}


```julia
short_answer();
```

## (b) {-}

```julia

up = rand(20:30);

```

$|A \cap B|$ when
\begin{equation*}
A = \{x \in \mathbb{N} : x \le `j up`\}
\end{equation*}
and

\begin{equation*}
B = \{x \in \mathbb{N} : x \text{ is not prime}\}.
\end{equation*}

```julia
short_answer();
```

# Logic (1 pt)

Consider the statement, 
``If you will give me a :panda:, then I will give you :unicorn:.''

Decide whether each statement below is the converse, 
the contrapositive, or neither.

1. If I will give you :unicorn:, then you will not give me a :panda:.
1. If I will not give you :unicorn:, then you will not give me a :panda:.
1. If you will not give me a :panda:, then I will not give you :unicorn:.
1. You will give me a :panda: and I will give you :unicorn:.

\smallskip

Answer: 

* Converse: $\underline{\hspace{5cm}}$
* Contrapositve: $\underline{\hspace{5cm}}$
* Neither: $\underline{\hspace{5cm}}$

# Logic (1 pt)

Simplify the following statements (so that negation only appears right
before variables).

## (a) {-}

\begin{equation*}
\neg (\neg P \implies  \neg Q).
\end{equation*}

```julia
short_answer();
```

## (b) {-}

\begin{equation*}
(\neg P \vee{} \neg Q) \implies  \neg (\neg Q \wedge{} \neg R).
\end{equation*}

```julia
short_answer();
```

