\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\title{Lecture 12 --- Generating Functions (2)}


\begin{document}

\maketitle

\section{AC 8 Generating functions}

\subsection{8.2 Another look at distributing apples or folders}

\begin{frame}
    \frametitle{Example 8.5}

    A grocery store is preparing holiday fruit baskets for sale. 

    Each fruit basket will have $n$ pieces of fruit in it, chosen from \emoji{apple}, \emoji{banana}, \emoji{carrot}, and \emoji{dumpling}.

    How many different ways can such a basket be prepared if 
    \begin{itemize}
        \item there must be at least one \emoji{apple} in a basket, 
        \item a basket cannot contain more than three \emoji{banana},
        \item and the number of \emoji{carrot} must be a multiple of four.
    \end{itemize}

\end{frame}

\begin{frame}[t]
    \frametitle{\ac{gf} for \emoji{apple}}

    Let $a_{n}$ be the number of ways to put in $n$ \emoji{apple} in the basket so that we have at least one \emoji{apple}.  

    The \ac{gf} for $(a_{n})_{n \ge 0}$ is
    \begin{equation*}
        A(x) = x + x^{2} + x^{3} + \cdots = \frac{x}{1-x}
    \end{equation*}
\end{frame}


\begin{frame}[t]
    \frametitle{\ac{gf} for \emoji{banana}}

    Let $b_{n}$ be the number of ways to put in $n$ \emoji{banana} in the basket so that we have at most three \emoji{banana}. 

    The \ac{gf} for $(b_{n})_{n \ge 0}$ is
    \begin{equation*}
        B(x) = 1 + x + x^{2} + x^{3}
    \end{equation*}
\end{frame}

\begin{frame}[t]
    \frametitle{\ac{gf} for \emoji{carrot}}

    Let $c_{n}$ be the number of ways to put in $n$ \emoji{carrot} in the basket so that we have a multiple of four \emoji{carrot}. 

    The \ac{gf} for $(c_{n})_{n \ge 0}$ is
    \begin{equation*}
        C(x) = 1 + x^{4} + x^{8} + \cdots = \frac{1}{1-x^{4}}
    \end{equation*}
\end{frame}

\begin{frame}[t]
    \frametitle{\ac{gf} for \emoji{dumpling}}

    Let $d_{n}$ be the number ways to put in $n$ \emoji{dumpling} in the basket.

    The \ac{gf} for $(d_{n})_{n \ge 0}$ is
        \begin{equation*}
            D(x) = 1 + x + x^{2} + \cdots = \frac{1}{1-x}
        \end{equation*}
\end{frame}

\begin{frame}[t]
    \frametitle{The \ac{gf} for fruit baskets}

    Let $e_{n}$ be the number of ways to put $n$ fruits in the basket so \emph{all}
    conditions are satisfied.

    The \ac{gf} for $(e_{n})_{n \ge 0}$ is
    \begin{equation*}
        \begin{aligned}
            \sum_{n = 0}^{\infty} e_{n} x^{n} 
            &
            \sum_{n = 0}^{\infty}
            \sum_{\substack{k_{1} k_{2} k_{3} k_{4}:\\
            k_{1} + k_{2} + k_{3} + k_{4} = n}}
            a_{k_{1}}
            b_{k_{2}}
            c_{k_{3}}
            d_{k_{4}}
            x^{n}
            \\
            &
            =
            A(x) B(x) C(x) D(x)
            \\
            &
            =
            \frac{x}{1-x}
            (1 + x + x^{2} + x^{3})
            \frac{1}{1-x^{4}}
            \frac{1}{1-x}
            \\
            &
            =
            \sum_{n = 0}
            \binom{n+1}{2}
            x^{n}
        \end{aligned}
    \end{equation*}

    The last step follows either by either by \emoji{muscle} or by
    \href{https://www.wolframalpha.com/input/?i=SeriesCoefficient\%5Bx\%2F\%281-x\%29*\%281\%2Bx\%2Bx\%5E2\%2Bx\%5E3\%29\%281\%2F\%281-x\%5E4\%29\%29\%281\%2F\%281-x\%29\%29\%2C+\%7Bx\%2C+0\%2C+n\%7D\%5D}{WolframAlpha}.
\end{frame}

%\begin{frame}
%    \frametitle{The Recipe \emoji{ramen} --- Integer composition with \ac{gf}}
%
%    The fruit basket problem is like asking the number of integer solutions of
%    \begin{equation*}
%        x_{1} + x_{2} + \cdots x_{k} = n
%    \end{equation*}
%    such that $x_{1},\ldots, x_{k}$ satisfies some restrictions.
%
%    We can solve such a problem by
%    \begin{itemize}
%        \item find the \ac{gf} for each variable (fruit) according to \emph{restrictions};
%        \item multiply them together to a new \ac{gf}\@;
%        \item extract the coefficients of the new \ac{gf}, either by \emoji{muscle} or by
%            \emoji{robot};
%        \item this gives the answer.
%    \end{itemize}
%\end{frame}

%\begin{frame}
%    \frametitle{\tps{}}
%    
%    Consider the equality
%    \begin{equation*}
%        x_1 + x_2 + x_{3} = n
%    \end{equation*}
%    where $x_1, x_{2}, x_3, n \ge 0$ are all integers. 
%
%    Suppose also that $x_1 \ge 2$, $x_2$ is a multiple of 5, and $0 \le x_3 \le 4$. 
%
%    Let $c_n$ be the number of solutions.
%
%    What is \ac{gf} for $c_n$?
%
%    Can you find a closed form of $c_n$?
%
%    What is $c_{20}$?
%\end{frame}

\section{8.4 Newton's Binomial Theorem}

\begin{frame}
    \frametitle{Definition 8.8}
    
    For all real numbers $p$ and nonnegative integers $k$, 
    the number $P(p, k)$
    is defined by
    \begin{enumerate}
        \item $P(p, 0) = 1$ for all real numbers $p$ and,
        \item $P(p, k) = p P(p − 1, k − 1)$ 
            for all real numbers $p$ and integers $k > 0$.
    \end{enumerate}

    \cake{} What is $P(\pi, 0)$, $P(\pi, 1)$, and $P(-1/2, 2)$?

\end{frame}

\begin{frame}[t]
    \frametitle{Definition 8.9 -- Binomial coefficients}

    For all real numbers $p$ and nonnegative integers $k$,
    \[
        \binom{p}{k}
        =
        \frac{P(p,k)}{k!}
    \]
    \emoji{smile} This is simply the binomial coefficient
    when $p \ge k \ge 0$ and $p, k \in \dsZ$?

    \cake{} What are $\binom{\pi}{0}$, $\binom{\pi}{1}$ and $\binom{-1/2}{2}$?
\end{frame}

\begin{frame}[t]{Binomial Theorem}
    \begin{block}{Theorem 2.30 (AC)}
        For all \(p \in \mathbb Z\) with \(p \ge 0\),
        \[
            (1+x)^{p} = \sum_{n = 0}^{p} \binom{p}{n} x^{n}
        \]
    \end{block}
\end{frame}

\begin{frame}[t]{Newton's Binomial Theorem}
    \begin{block}{Theorem 8.10 (AC)}
        For all \(p \in \mathbb R\) with \(p \ne 0\),
        \[
            (1+x)^{p} = \sum_{n = 0}^{\infty} \binom{p}{n} x^{n}
        \]
    \end{block}

    For a proof, see
    \href{https://math.libretexts.org/Bookshelves/Combinatorics_and_Discrete_Mathematics/Combinatorics_and_Graph_Theory_\%28Guichard\%29/03\%3A_Generating_Functions/3.02\%3A_Newton\%27s_Binomial_Theorem}{here}.
    
    \think{} Why is this the same as Binomial Theorem when $p \in \dsN$?
\end{frame}

\subsection{8.4 An Application of the Binomial Theorem}

\begin{frame}[t]{Applying Newton's Binomial Theorem}
    \begin{block}{Lemma 8.12 (AC)}
        For all integers \(k \ge 0\), 
        \vspace{-1em}
        \[
            \binom{-1/2}{k}=(-1)^{k} \frac{\binom{2 k}{k}}{2^{2 k}}. 
        \]
        \vspace{-1em}
    \end{block}

    \pause{}

    \begin{block}{Theorem 8.13 (AC)}
        \[
            \frac{1}{\sqrt{1-4x}}
            =
            \sum_{n \ge 0} \binom{2n}{n} x^n
            .
        \]
        \vspace{-1em}
    \end{block}
\end{frame}

\begin{frame}[t]{An identity}
    \begin{block}{Corollary 8.14 (AC)}
    For all integers \(n \ge 0\)
    \begin{equation*}
        2^{2n} = \sum_{k \ge 0} \binom{2k}{k} \binom{2n-2k}{n-k}.
    \end{equation*}
    \end{block}
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-12.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \courseassignment{}

            \href{https://www.rellek.net/book/app-comb.html}{Applied Combinatorics}
            \begin{itemize}
                \item[\emoji{pencil}] Section 8.8: 5, 7, 9, 11, 13, 15.
            \end{itemize}

            \hint{} For some problems, you can use WolframAlpha like 
            \href{https://www.wolframalpha.com/input/?i=SeriesCoefficient\%5Bx\%2F\%281-x\%29*\%281\%2Bx\%2Bx\%5E2\%2Bx\%5E3\%29\%281\%2F\%281-x\%5E4\%29\%29\%281\%2F\%281-x\%29\%29\%2C+\%7Bx\%2C+0\%2C+n\%7D\%5D}{this}.
        \end{column}
    \end{columns}
\end{frame}

\end{document}
