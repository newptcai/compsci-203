\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\title{Lecture 22}

\begin{document}

\maketitle

\lectureoutline{}

\section{AC 5.4.3 Can We Determine Chromatic Number?}

\subsection{First-Fit Algorithm}

\begin{frame}
    \frametitle{Computational Complexity}
    
    \emoji{worried} Deciding if $\chi(G) = k$ is
    \href{https://en.wikipedia.org/wiki/NP-complete}{NP-complete} 
    i.e., very hard, except for the cases $k \in \{0,1,2\}$. 

    \cake{} Why it is easy for $k \in \{0, 1\}$?

    \think{} Can you think of an algorithm to decide if $\chi(G) = 2$?
\end{frame}

\begin{frame}
    \frametitle{Let's colour a graph}
    
    \cake{} Without thinking much, how to find a \emph{proper} 
    colouring of this graph?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\textwidth]{first-fit-01.pdf}
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{First-fit algorithm}

    A ``naive'' algorithm to find a proper colouring ---
    \begin{itemize}
        \item Fix an ordering of the vertex set $\{v_1 , v_2 , . . . v_n\}$.
        \item Let $\phi(v_{1}) = 1$.
        \item Given of $\phi(v_1), \phi(v_2), \ldots, \phi(v_i)$, let $\phi(v_i+1)$
            be the smallest integer (colour) allowed.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Colouring a graph greedily \emoji{heart-eyes}}

    \think{} What happens when first-fit is applied to the same graph,
    but with different orders of vertices?
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{colouring-2.png}
    \end{figure}
\end{frame}

\subsection{Maximum Degrees and Chromatic Numbers}

\begin{frame}
    \frametitle{The maximum degree}
    
    Let $\Delta(G)$ be the maximum degree in $G$.

    \cake{} What is $\Delta(G)$ and $\chi(G)$ for this $G$?

    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.6\textwidth]{first-fit-03.pdf}
        \includegraphics<2>[width=0.6\textwidth]{first-fit-04.pdf}
        \includegraphics<3>[width=0.6\textwidth]{first-fit-02.pdf}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Brook's Theorem}

    \begin{block}{Theorem 4.4.5 (DM)}
        Any graph $G$ satisfies $χ(G) \le ∆(G)$, 
        unless $G$ is a complete graph or an odd cycle, 
        in which case $χ(G) = ∆(G) + 1$.
    \end{block}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.32\textwidth]{first-fit-03.pdf}
        \includegraphics[width=0.32\textwidth]{first-fit-04.pdf}
        \includegraphics[width=0.32\textwidth]{first-fit-02.pdf}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{A simpler bound}

    \begin{block}{\astonished{} Fact}
        First-fit algorithm guarantees that $\chi(G) \le \Delta(G) + 1$.
    \end{block}

    \only<1>{
        \cake{} What is $\Delta(G)$ and $\chi(G)$?

        \begin{figure}[htpb]
            \centering
            \includegraphics[width=0.4\textwidth]{first-fit-06.pdf}
            \caption*{$G$}
        \end{figure}
    }

    \only<2>{
        \cake{} How many colours does first-fit need?

        \vspace{-3em}

        \begin{figure}[htpb]
            \centering
            \includegraphics[width=0.8\textwidth]{first-fit-05.pdf}
            \vspace{-3em}
            \caption*{$G$}
        \end{figure}
    }
\end{frame}

\subsection{Interval Graphs}

\begin{frame}
    \frametitle{How many film can you watch?}

    You are at a \emoji{film-frames} festival which screens many films.

    The screening times are shown as line segments below.

    \think{} What is the max number of films which you can watch in their entirety?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{interval.png}
    \end{figure}

    \hint{} What is the maximum $I_{n}$ you can find the graph?
\end{frame}

\begin{frame}[c]
    \frametitle{Interval graphs}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{interval.png}
    \end{figure}

    Given a list of intervals $(S_{v})_{v \in V}$, the \emph{interval graph} is the
    graph with vertex set $V$ and edge set
    \begin{equation*}
        E = \{ uv : S_{u} \cap S_{v} \ne \emptyset\}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    \cake{} Can You Draw the Interval Graph $G$?

    \think{} What is $\omega(G)$ and $\chi(G)$?

    \bigskip
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{Interval_graph.svg.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{The clique number of an interval graph}

    Try colouring this graph with first-hit.
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.51\linewidth]{interval-2.pdf}
        \includegraphics[width=0.51\linewidth]{interval-1.pdf}
    \end{figure}
\end{frame}

\begin{frame}[t]
    \frametitle{When does chromatic number equal clique number?}

    \begin{block}{Theorem 5.28 (AC)}
        If $G = (V, E)$ is an interval graph, then $\chi(G) = \omega(G)$.
    \end{block}
\end{frame}

\subsection{Perfect Graphs}

\begin{frame}
    \frametitle{Perfect Graphs}
    
    A graph $G$ is said to be \alert{perfect} if $\chi(H) = \omega(H)$ for every
    \emph{induced} subgraph $H$. 

    \cake{} Is the following graph is perfect?
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.45\linewidth]{interval-2.pdf}
        \includegraphics[width=0.45\linewidth]{interval-1.pdf}
    \end{figure}

    \sweat{} Are all interval graphs perfect? Why?
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    \bonus{} Can you give \alert{two} different arguments 
    that $K_{n}$ is perfect for any $n$?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\linewidth]{interval-5.pdf}
        \caption*{$K_{6}$}%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Complement graph}

    Let $G = (V, E)$ be a graph and let $K = C(V,2)$, i.e., the set of all
    $2$-element subset of $V$. 

    Then $H = (V, K \setminus E)$ is the \alert{complement} of G.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\textwidth]{c5.pdf}
        \includegraphics[width=0.4\textwidth]{c5-complement.pdf}
        \caption*{$C_5$ and its complement}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{The Strong Perfect Graph Theorem}
    
    \astonished{} A graph is perfect if and only if it has neither odd holes 
    (odd-length \emph{induced} cycles of length at least $5$) nor odd anti-holes (complements of odd holes).

    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.4\textwidth]{c5.pdf}
        \includegraphics<1>[width=0.4\textwidth]{c5-complement.pdf}
        \includegraphics<2>[width=0.4\textwidth]{c7.pdf}
        \includegraphics<2>[width=0.4\textwidth]{c7-complement.pdf}
        \caption*{A hole and an anti-hole}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{The complete graph is perfect (again)!}

    \think{} Why $K_{n}$ has neither a hole nor an anti-hole? (So it's perfect!)

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\linewidth]{interval-5.pdf}
        \caption*{$K_{6}$}%
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{Maria Chudnovsky}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            The theorem was proved by
            \href{https://en.wikipedia.org/wiki/Maria\_Chudnovsky}{Maria Chudnovsky},
            Neil Robertson, Paul Seymour, and
            Robin Thomas (2006).

            \bigskip

            Chudnovsky won a MacArthur Fellowship (The Genius Grant) in 2012, which comes
            with a grant 500,000 \emoji{dollar}, no strings attached.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.7\linewidth]{Chudnovsky.jpg}
                \caption*{Maria Chudnovsky}%
            \end{figure}

        \end{column}
    \end{columns}
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-22.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \bomb{} Assignment problems will \emph{not} be graded but they will appear in quizzes!

            \href{https://www.rellek.net/book/app-comb.html}{Applied Combinatorics}
            \begin{itemize}
                \item[\emoji{pencil}] Section 5.9: 19, 21, 22, 23.
            \end{itemize}

            \href{http://discrete.openmathbooks.org/dmoi3}{Discrete Mathematics}, 
            \begin{itemize}
                \item[\emoji{pencil}] 
                    Section 4.4: 1, 2, 3, 5, 7, 9, 10, 12.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
