\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

%\includeonlyframes{current}

\title{Lecture 03}

\begin{document}

\maketitle

\begin{frame}
    \frametitle{Summary}
    \tableofcontents{}
\end{frame}

\section{3.1 Propositional Logic}

\begin{frame}
    \frametitle{Definitions}
    
    A \alert{proposition} is simply a statement. 

    \alert{Propositional logic} studies the ways statements can interact with each other.

     It does not really care about the content of the statements.

     For example, the following are all $P \imp Q$.
     \begin{itemize}
         \item If the \emoji{moon} is made of \emoji{cheese},
             then \emoji{basketball} are round. 
         \item If \emoji{spider} have eight \emoji{leg},
             then Sam walks with a \emoji{balloon}.
         \item If \emoji{cat-face} can fly,
             they will rule the \emoji{earth-asia}.
     \end{itemize}
\end{frame}

\subsection{Truth Tables}

\begin{frame}[c]
    \frametitle{Example}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            
            \begin{exampleblock}{Sam says ---}
            In the Game of Monopoly, if you get more doubles than any other player
            then you will lose, or if you lose then you must have bought the most
            properties.
            \end{exampleblock}

            \think{} Is this proposition true?

            %This is to ask when is  $(P \imp Q) \wedge (Q \imp R)$ true.
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\linewidth]{pexels-cottonbro-4004174.jpg}
                \caption*{By Cottonbro from Pexels}%
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Truth tables}
    
    In a \alert{truth table}, each row lists a possible combination of $T$'s and $F$'s, 
    and then marks down if a statement is true or false in this case.
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{truth-table-1.png}
        \includegraphics[width=0.18\linewidth]{truth-table-2.png}
        \caption*{Truth tables of logical connectives}%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 3.1.1}
    
    The truth table of $\neg P \vee Q$ is
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.36\linewidth]{truth-table-3.png}
    \end{figure}
    Compare this with $P \imp Q$.
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.36\linewidth]{truth-table-1-1.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{The Monopoly problem}
    
    \begin{exampleblock}{Sam says ---}
        If you get more doubles than any other player then you will lose,
        or if you lose then you must have bought the most properties.
    \end{exampleblock}

    This is simply $(P \imp Q) \vee (Q \imp R)$, whose truth table is

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{truth-table-4-1.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{$\Xor$}

    We define one more logic connective $\Xor$ using the following truth table.
    
    \begin{equation*}
        \begin{array}{c|c|c}
            \hline
            P & Q & \Xor(P,Q)  \\
            \hline
            T & T & F  \\
            T & F & T  \\
            F & T & T  \\
            F & F & F  \\
            \hline
        \end{array}
    \end{equation*}
\end{frame}

\begin{frame}[t]
    \frametitle{Knights and Knaves}

    Two trolls, A and B, stand before you. 

    A says, ``We are both knaves.'' 

    It is easy to see that $A$ is a knave and $B$ is knight? 

    \cake{} But can we solve this with truth table?
\end{frame}

\begin{frame}[t]
    \frametitle{\tps{} --- Knights and Knaves}

    You stumble upon another two trolls. They tell you:

    \begin{itemize}
        \item Troll A: If we are cousins, then we are both knaves.
        \item Troll B: We are cousins or we are both knaves.
    \end{itemize}

    \cake{} What should C, SA and SB represent in the following table?

    \begin{equation*}
        \small
        \begin{array}{c|c|c|c|c}
            \hline
            \text{A} & \text{B} & \text{C} & \text{SA} & \text{SB} \\
            \hline
            T & T & T & F & T \\
            T & T & F & T & F \\
            T & F & T & F & F \\
            T & F & F & T & T \\
            F & T & T & T & T \\
            F & T & F & F & F \\
            F & F & T & F & F \\
            F & F & F & F & F \\
            \hline
        \end{array}
    \end{equation*}
\end{frame}

\subsection{Logical Equivalence}

\begin{frame}
    \frametitle{Logical equivalence}
    
    Two (molecular) statements $P$ and $Q$ are \alert{logically equivalent} if $P$ is
    true precisely when $Q$ is true.

    For example, $P \imp Q$ and $\neg P \vee Q$ are logically equivalent.
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\linewidth]{truth-table-5.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Snow and rain}
    
    Are the statements
    \begin{itemize}
        \item it will not \emoji{cloud-with-rain} or \emoji{cloud-with-snow}
        \item it will not \emoji{cloud-with-rain} and it will not
            \emoji{cloud-with-snow}
    \end{itemize}
    logically equivalent?
    
    \hint{} Filling in the following truth table ---
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\linewidth]{truth-table-6.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Boolean algebra}
    
    \begin{exampleblock}{De Morgan's Laws}
        \begin{itemize}
            \item $\neg (P \wedge Q) \iff \neg P \vee \neg Q$.
            \item $\neg (P \vee Q) \iff \neg P \wedge \neg Q$.
        \end{itemize}
    \end{exampleblock}

    \begin{exampleblock}{Implications are Disjunctions}
        \begin{itemize}
            \item $P \imp Q \iff \neg P \vee Q$.
        \end{itemize}
    \end{exampleblock}


    \begin{exampleblock}{Double Negation}
        \begin{itemize}
            \item $\neg \neg P \iff P$. (It is not the case that Sam is not a \emoji{dog} =
                Sam is a \emoji{dog})
        \end{itemize}
    \end{exampleblock}

    We can apply such logical equivalence to transform one statement into another.
\end{frame}

\begin{frame}[t]
    \frametitle{Negation of an Implication}

    Prove that the statements $\neg (P \imp Q)$ and $P \wedge \neg Q$ are logically
    equivalent.
\end{frame}

\begin{frame}
    \frametitle{Verifying equivalence}

    Are these two statements equivalent?
    \begin{itemize}
        \item $(P \vee Q) \imp R$
        \item $(P \imp R) \vee (Q \imp R)$
    \end{itemize}

    \bomb{} Transformation rules can prove equivalence, but \emph{cannot} disapprove it.
\end{frame}

\begin{frame}
    \frametitle{The advantage of truth table}
    
    However, truth tables can show that two statements
    are \emph{NOT} equivalent.
    
    \begin{exampleblock}{Two non-equivalent statements}
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{truth-table-7.png}
    \end{figure}
    \end{exampleblock}
\end{frame}

\subsection{Deductions}

\begin{frame}
    \frametitle{Who gets a \emoji{cookie}?}
    Consider two arguments
    \[
        \begin{array}{ r l }
               & \text{If Edith eats her \emoji{apple}, then she can have a \emoji{cookie}.} \\
               & \text{Edith eats her \emoji{apple}.} \\
               \cline{2-2}
            \therefore & \text{Edith gets a \emoji{cookie}.}
        \end{array}
    \]
    \pause{}
    and
    \[
        \begin{array}{ r l }
               & \text{Florence must eat her \emoji{apple} in order to get a \emoji{cookie}.} \\
               & \text{Florence eats her \emoji{apple}.} \\
               \cline{2-2}
            \therefore & \text{Florence gets a \emoji{cookie}.}
        \end{array}
    \]
    Are these \alert{valid} arguments?
\end{frame}

\begin{frame}[c]
    \frametitle{Valid and invalid arguments}
    
    An \alert{argument} is a set of statements, one of which is called the
    \alert{conclusion} and the rest of which are called \alert{premises}. 

    An argument is \alert{valid} if the conclusion must be true whenever the
    premises are all true. 

    An argument is \alert{invalid} if it is
    \emph{possible} for all the premises to be true and the conclusion to be false.
\end{frame}

\begin{frame}[c]
    \frametitle{\dizzy{} Sound and unsound arguments}
    
    The following argument \emph{is} valid,
    \[
        \begin{array}{ r l }
               & \text{All \emoji{wastebasket} are items made of gold.} \\
               & \text{All items made of gold are \emoji{clock9}-travel devices.} \\
               \cline{2-2}
            \therefore & \text{All \emoji{wastebasket} are \emoji{clock9}-travel devices.}
        \end{array}
    \]
    But it is \alert{unsound}, because the premises are false.

    \bomb{} In mathematics, we only care about \emph{validity}, but in life you should also
    consider \emph{soundness}.

    \emoji{eye} \url{https://iep.utm.edu/val-snd/}
\end{frame}

\begin{frame}
    \frametitle{Deduction rule}
    
    \small
    The argument
    \[
        \begin{array}{ r l }
               & \text{If Edith eats her \emoji{apple}, then she can have a \emoji{cookie}.} \\
               & \text{Edith eats her \emoji{apple}.} \\
               \cline{2-2}
            \therefore & \text{Edith gets a \emoji{cookie}.}
        \end{array}
    \]
    has the form
    \[
        \small
        \begin{array}{ r l }
               & P \imp Q \\
               & P \\
               \cline{2-2}
            \therefore & Q
        \end{array}
    \]
    This is a \alert{deduction rule}, i.e., an argument form which is
    always \emph{valid}. 
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.25\linewidth]{truth-table-8.png}
    \end{figure}
\end{frame}

%\begin{frame}
%    \frametitle{Deduction rule 1}
%    
%    \begin{columns}
%        \begin{column}{0.4\textwidth}
%    
%    If our axioms are
%    \begin{enumerate}
%        \item $P$
%        \item $P \imp Q$
%    \end{enumerate}
%    then $Q$ is true.
%            
%        \end{column}
%        \begin{column}{0.6\textwidth}
%            
%    If we our axioms are
%    \begin{enumerate}
%        \item Sam is a \emoji{dog}.
%        \item If Sam is a \emoji{dog}, then Sam is cute.
%    \end{enumerate}
%    Then \emph{Sam is cute} is true.
%        \end{column}
%    \end{columns}
%\end{frame}

\begin{frame}
    \frametitle{Deduction rule --- We like Sam}

    \begin{columns}
        \begin{column}{0.3\textwidth}
            Deduction rule ---
            \[
                \begin{array}{ r l }
               & P \imp Q \\
               & Q \imp R \\
               \cline{2-2}
                    \therefore & P \imp R
                \end{array}
            \]

        \end{column}
        \begin{column}{0.7\textwidth}
            Example ---
            \[
                \begin{array}{ r l }
               & \text{If Sam is a \emoji{dog}, then Sam is cute.} \\
               & \text{If Sam is cute, then we like Sam.} \\
               \cline{2-2}
                    \therefore & \text{If Sam is a \emoji{dog}, then we like Sam.}
                \end{array}
            \]
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Deduction rule --- Sam is not a \emoji{dog}}
    \begin{columns}
        \begin{column}{0.3\textwidth}
            Deduction rule ---
            \[
                \begin{array}{ r l }
               & P \imp Q \\
               \cline{2-2}
                    \therefore & \neg Q \imp \neg P
                \end{array}
            \]

        \end{column}
        \begin{column}{0.7\textwidth}
            Example ---
            \[
                \begin{array}{ r l }
               & \text{If Sam is a \emoji{dog}, then Sam is cute.} \\
               \cline{2-2}
                    \therefore & \text{If Sam is \emph{not} cute, then Sam is \emph{not} a \emoji{dog}.}
                \end{array}
            \]
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Deduction rule --- We always like Sam}
    \begin{columns}
        \begin{column}{0.3\textwidth}
            Deduction rule ---
            \[
                \begin{array}{ r l }
               & P \imp Q \\
               & \neg P \imp Q \\
               \cline{2-2}
                    \therefore & Q
                \end{array}
            \]

        \end{column}
        \begin{column}{0.7\textwidth}
            Example ---
            \[
                \begin{array}{ r l }
               & \text{\small If Sam is a \emoji{dog}, then we like Sam.} \\
               & \text{\small If Sam is \emph{not} a \emoji{dog}, then we like Sam.} \\
               \cline{2-2}
                    \therefore & \text{\small We like Sam}
                \end{array}
            \]
        \end{column}
    \end{columns}

    Show the above rule is valid by filling in the truth table.
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\linewidth]{truth-table-9.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    Is the following a valid deduction rule?

    \[
        \begin{array}{ r l }
        & A \imp D \\
        & B \imp D \\
        & C \imp D \\
        & D \\
        \cline{2-2}
            \therefore & A \vee B \vee C
        \end{array}
    \]

    \hint{} You don't need to fill the entire truth-table.
\end{frame}

\subsection{Beyond Propositions}

\begin{frame}
    \frametitle{Predicate Logic}
    
    We may want to prove a statement like
    \begin{itemize}
        \item All primes greater than 2 are odd.
    \end{itemize}
    This can be written as
    \begin{equation*}
        \forall x((P(x) \wedge x > 2) \imp O(x))
    \end{equation*}
    where $P(x)$ denotes $x$ is a prime and $O(x)$ denotes $x$ is odd.

    $P(x)$ and $O(x)$ are not propositions but \alert{predicates}.

    The logic studying statements involving predicates is called \alert{predicate logic}.
\end{frame}

\begin{frame}
    \frametitle{Logical equivalence in predicate logic}
    
    There is no analogues to truth-tables for predicate logic.
    All we can do is to use transformation rules (logical equivalence).

    \begin{exampleblock}{Example of logical equivalence}
        ``There is no smallest number'' can be written as
        \begin{equation*}
            \neg \exists x \forall y (x \le y).
        \end{equation*}
        We can transform this into
        \begin{equation*}
            \forall x \exists y (y < x),
        \end{equation*}
        i.e.,
        ``for every $x$ there is a $y$ which is smaller than $x$''.
    \end{exampleblock}
\end{frame}

\begin{frame}[c]
    \frametitle{What is a proof?}

    \begin{enumerate}
        \item We start with a set of statements known as \alert{axioms}. An axiom is
            a basic statement that is simply accepted as \emph{true}.
        \item We apply logical \emph{deduction rules} to the axioms, and the
            resulting statements, until we arrive at the \emph{statement} we wish to prove.
    \end{enumerate}
\end{frame}

\appendix{}

\begin{frame}
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-03.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \bomb{} Assignment problems will \emph{not} be graded but they will appear in quizzes!

            \href{http://discrete.openmathbooks.org/dmoi3}{Discrete
            Mathematics}, 
            \begin{itemize}
                \item[\emoji{pencil}] Section 3.1: 1-6, 8, 10-12.            
            \end{itemize}
            \href{https://puzzlewocky.com/brain-teasers/knights-and-knaves/}{Knights
            and Knaves Puzzles.}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[c]
    \frametitle{\dizzy{} A Famous Argument}

    The \href{https://www.youtube.com/watch?v=gGczdp0SE0c}{drowning child argument} 
    is a famous thought experiment
    introduced by the moral philosopher \href{https://en.wikipedia.org/wiki/Peter\_Singer}{Peter Singer}.

    \think{} Is it a valid argument? Is it sound?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{drowning-child.jpg}
    \end{figure}
\end{frame}

\end{document}
