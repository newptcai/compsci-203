\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}
\input{../tikz.tex}

\title{Lecture 23}

\begin{document}

\maketitle

\lectureoutline{}

\section{AC 5.5 Planar Graphs}

\subsection{Definitions}

\begin{frame}[c]
    \frametitle{Homes and utilities}

    \think{} Can we connect three homes to three types of utilities so that the pipes and
    lines do not cross each other?

    \bigskip{}

    \begin{figure}[htpb]
        \centering
        \begin{tikzpicture}
            [
            scale=1.50,
            inner sep=1.5mm,
            vertex/.style={opacity=0,text opacity=1,font=\huge},
            every path/.style={lightgray,ultra thick},
            ]
            \node (water) at (0,1) [vertex] {\emoji{droplet}};
            \node (electricity) at (0,2) [vertex] {\emoji{zap}};
            \node (natural gas) at (0,3) [vertex] {\emoji{fire}};
            \node (home 1) at (4,3) [vertex] {\emoji{house}};
            \node (home 2) at (4,2) [vertex] {\emoji{hut}};
            \node (home 3) at (4,1) [vertex] {\emoji{house-with-garden}};
            %\def\utilities{water,electricity,natural gas}
            %\def\homes{1, 2, 3}
            %\foreach \u in \utilities{%
            %    \foreach \h in \homes{%
            %        \draw (\u.east) -- (home \h.west);%
            %    }
            %}
        \end{tikzpicture}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Planar graphs}
    
    A \alert{planar graph} is a graph that can be drawn on the plane in a way that 
    its edges intersect only at their endpoints. 

    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\linewidth]{planar-k-3-3.pdf}
                \caption{$K_{3,3}$ -- Not a planar graph}%
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\linewidth]{planar-k-4.pdf}
                \caption{$K_{4}$ -- A planar graph}%
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Faces of a planar graph}

    A \alert{face} of a planar drawing of a planar graph is a region bounded by edges and
    vertices and not containing any other vertices or edges.
    
    \cake{} How many faces does this graph have?
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.35\linewidth]{planar-face-1.png}
        \caption{A planar graph}%
    \end{figure}
\end{frame}

\subsection{Euler's Formula}

\begin{frame}
    \frametitle{Faces, edges, and vertices}

    Let $f$, $n$, $m$ be the number faces, vertices, and edges of a graph.

    \cake{} Can you guess a relationship among $f$, $n$, $m$?

    \begin{figure}[htpb]
        \centering
        \includegraphics<1>[width=0.4\textwidth]{planar-k-4.pdf}
        \includegraphics<2>[width=0.4\textwidth]{planar-face-2.pdf}
        \caption{\only<1>{$K_4$}\only<2>{A planar graph}}
    \end{figure}
\end{frame}

\begin{frame}[t]
    \frametitle{Euler's Formula}

    \begin{block}{Theorem 5.32 (AC)}
        Let $G$ be a connected planar graph with $n$ vertices and $m$ edges. 
        Every planar drawing of $G$ has $f$ faces, where $f$ satisfies 
        $\blankshort{} = \blankveryshort{}$.
    \end{block}

    \pause{}

    Proof by induction on $m$.

    Base case: $m = 0$.

    Assume the theorem holds for all planar graphs with $m-1$ edges.

    Consider a connected planar graph $G$ with $m$ edges. 

    Let $G'$ be $G$ with one edge removed.
\end{frame}

\begin{frame}[t]
    \frametitle{Euler's Formula}
    Case 1: $G'$ is connected.
\end{frame}

\begin{frame}[t]
    \frametitle{Euler's Formula}

    Case 2: $G'$ is not connected.
\end{frame}

\subsection{Is this graph planar? -- A Simple Check}

\begin{frame}
    \frametitle{Edges and faces}

    Let $(e, F)$ be an edge $e$ and $F$ be a face adjacent to it.

    Let $p$ denote the number of such pairs in $G$.

    \cake{} What are $p, m, f$ in $G$?
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\textwidth]{planar-face-3.pdf}
        \caption{A planar graph $G$}
    \end{figure}
\end{frame}

\begin{frame}[t]
    \frametitle{A corollary}
    
    \begin{block}{Theorem 5.33 (AC)}
        A planar graph on n vertices has at most $3n − 6$ edges when $n \ge 3$.
    \end{block}

    The proof uses the inequality
    \begin{equation*}
        \blankveryshort{} \cdot f \le p \le \blankveryshort{} \cdot m
    \end{equation*}

    \pause{}

    \emoji{laughing} This implies $K_{5}$ is not planar. 

    \cake{} What about $K_{n}$ with $n > 5$? Are they planar?
\end{frame}

\begin{frame}[t]
    \frametitle{$K_{3,3}$ is not planar}

    \weary{} $K_{3,3}$ passes the test of Theorem 5.33.
    How to show that it is not planar?

    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\linewidth]{planar-k-3-3.pdf}
                \caption{$K_{3,3}$ -- Not a planar graph}%
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            Proof by contraction.
        \end{column}
    \end{columns}
\end{frame}


\subsection{Is this graph planar? -- Kuratowski's Theorem}

\begin{frame}
    \frametitle{Elementary subdivision}

    Given $G$, we get a \alert{elementary subdivision} of $G$ by splitting one edge into two
    with a new vertex.
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.45\linewidth]{k4.pdf}
        \includegraphics[width=0.45\linewidth]{k4-subdivision.pdf}
        \caption{$K_{4}$ and its subdivision}%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Homeomorphism}

    Two graphs $G_{1}$ and $G_{2}$ are \alert{homeomorphic} 
    if they can be reach from
    the same graph $G$ by subdivisions.
    
    \cake{} Which two the following are homeomorphic?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.3\linewidth]{k4-subdivision-1.pdf}
        \includegraphics[width=0.3\linewidth]{k4-subdivision-2.pdf}
        \includegraphics[width=0.3\linewidth]{k4-subdivision-3.pdf}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{When is a graph planar?}

    \begin{block}{Theorem 5.34 (Kuratowski's Theorem)}
    A graph is planar if and only if it does not contain a 
    \emph{subgraph} homeomorphic to
    either $K_5$ or $K_{3,3}$.
    \end{block}

    How to show the following graph is not planar?
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\linewidth]{k33-planar.pdf}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}

    Show that Petersen graph is not planar.

    \hint{} Find a \emph{subgraph} which is homeomorphic to either $K_{3,3}$.

    \hint{} Can you find a subgraph of $C_{6}$ in both graphs?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.35\linewidth]{Peterson.png}
        \includegraphics[width=0.35\linewidth]{planar-k-3-3.pdf}
        \caption{Peterson graph and $K_{3,3}$}%
    \end{figure}
\end{frame}

\subsection{Four colour theorem}

\begin{frame}[c]
    \frametitle{Four colour theorem}
    
    \begin{block}{Theorem 5.37 (Four Color Theorem)}
    Every planar graph has chromatic number at most four.
    \end{block}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{map.png}
        \caption{From \href{https://en.wikipedia.org/wiki/Four_color_theorem\#/media/File:Map_of_United_States_vivid_colors_shown.png}{Wikipedia}.}%
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{History of the four colour theorem}

    First conjectured on October 23, 1852 by Francis Guthrie.

    First proved with \emoji{laptop} 1976 by Kenneth Appel.

    A simpler proof with \emoji{laptop} in 1997 by Robertson, Sanders, Seymour, and Thomas.

    In 2005, the theorem was also proved by Georges Gonthier with
    \href{https://en.wikipedia.org/wiki/Coq}{Coq}.
\end{frame}

\appendix{}

\begin{frame}
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-23.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \courseassignment{}

            \href{https://www.rellek.net/book/app-comb.html}{Applied Combinatorics}
            \begin{itemize}
                \item[\emoji{pencil}] Section 5.9: 29, 31, 33, 34, 35.
            \end{itemize}

            \href{http://discrete.openmathbooks.org/dmoi3}{Discrete Mathematics}, 
            \begin{itemize}
                \item[\emoji{pencil}] 
                    Section 4.3: 1, 2, 3, 5, 6, 8, 12, 14, 15.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
