\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}

\title{Lecture 14}

\begin{document}

\maketitle

\lectureoutline{}

\section{AC 9 Recurrence Equations}

\subsection{9.1 Introduction}

\begin{frame}
    \frametitle{How to divide a land into kingdoms \emoji{crown}}

    We draw 4 lines to divide a continent into kingdoms.

    \emph{No} point belongs to more than two lines.

    How many kingdoms do we get?

    \begin{figure}
        \centering
        \includegraphics[width=0.7\textwidth]{lines.png}
    \end{figure}

    Solution: $r_{n+1} = r_{n} + n + 1$.
\end{frame}

\begin{frame}
    \frametitle{Ternary strings with an even number of \emoji{carrot}}

    Let $d_{n}$ be the number of strings of length $n$ on the alphabet 
    $\{\temoji{apple}, \temoji{banana}, \temoji{carrot}\}$ with even numbers of
    \emoji{carrot}.
    
    \think{} What recursion does $d_{n}$ satisfy?
\end{frame}

\subsection{9.2 Linear Recurrence Equations}

\begin{frame}{Linear recurrence equations}
    A sequence \( (a_{n}, n\ge 0)\) satisfies a linear recurrence if
    \[
        c_{0} a_{n+k} + c_{1} a_{n+k-1} + c_{2} a_{n+k-2} + \dots + c_{k} a_{n} = g(n),
    \]
    where \(k \ge 1\) is an integers, \(c_{0}, c_{1},\dots,c_{k}\)  are constants, with
    \(c_{0}, c_{k} \ne 0\), and \(g\) is a function.

    \pause{}

    The recursion is \alert{homogeneous} if \(g(n)\) is always \(0\), like the
    Fibonacci sequence,
    \[
        f(n+2) - f(n+1) - f(n) = 0.
    \]
    Otherwise it is \alert{nonhomogeneous}, like the \emoji{crown} sequence.
    \[
        r(n+1)-r(n)=n+1.
    \]
\end{frame}

\begin{frame}{Advancement operator}
    Let \(A f(n)=f(n+1)\) and \(A^{p}f(n)=f(n+p)\).

    The recursion for Fibonacci sequence can be written as
    \[
        A^{2} f(n) - A f(n) - A^{0} f(n) = 0
    \]
    or equivalently
    \[
        (A^{2}- A  - 1) f = 0.
    \]
\end{frame}

\begin{frame}{Advancement operator}
    The recurrence 
    \[
        c_{0} f({n+k}) + c_{1} f({n+k-1}) + c_{2} f({n+k-2}) + \cdots + c_{k} f({n}) = g(n),
    \]
    can be written as
    \[
        p(A) f = 
        (c_{0} A^{k} + c_{1} A^{k-1} + c_{2} A^{k-2} + \cdots + c_{k} ) f = g.
    \]
\end{frame}

\begin{frame}
    \frametitle{Polynomials of advancement operator}

    \cool{} If $p(A) = q(A)$ holds when both sides are seen as polynomials,
    then $p(A)f = q(A)f$.

    For example, seen as polynomials
    \begin{equation*}
        (A-2)(A + 3) = A^{2} + A - 6.
    \end{equation*}
    Then
    \begin{equation*}
        (A-2)(A + 3) f = (A^{2} + A - 6)f.
    \end{equation*}
\end{frame}

\subsection{9.4.1 Solving Advancement Operator Equations -- Homogeneous Case}

\begin{frame}[t]{A trivial example}
    Find \alert{all} solutions for
    \[
        (A-2) f({n)} = 0
    \]

    \pause{}

    Find \alert{all} the solution for 
    \[ 
        (A+3) f(n) = 0
    \]
\end{frame}

\begin{frame}[t]{Example 9.9}
    Find all solutions for
    \begin{equation}
        \label{eq:homogeneous:1}
        p(A) f 
        = (A^{2}+A-6) f 
        = (A+3)(A-2)f 
        = 0
    \end{equation}

    \pause{}

    \hint{} \emph{Fact} ---
    \begin{itemize}
        \item Then \(c_{1} 2^{n} + c_{2} (-3)^{n}\) are also solutions of \cref{eq:homogeneous:1}.
        \item \alert{All} solutions are of this form.
    \end{itemize}
\end{frame}

\begin{frame}[t]
    \frametitle{A recipe \emoji{ramen} for homogeneous equations}
    To solve $p(A) f= 0$, we first factorize
    \[
        p(A) = (A-r_{1})(A-r_{2})(A-r_{3}) \dots (A-r_{k}).
    \]
    This can always be done in $\mathbb{C}$.

    The numbers $r_{1}, \dots, r_{k}$ are called the \alert{roots} of $p(A)$.

    \begin{block}{Theorem 9.21 (AC)}
        When $r_{1}, \dots, r_{n}$ are \emph{distinct},
        \alert{all} the solutions of $p(A) f = 0$ are of the form
        \begin{equation*}
            c_{1} r_{1}^{n}
            +
            c_{2} r_{2}^{n}
            +
            \cdots
            +
            c_{k} r_{k}^{n}
        \end{equation*}
    \end{block}
\end{frame}

\begin{frame}[t]{Another example}
    Find all the solutions for
    \[
        t(n+2)-3t(n+1)+t(n) = 0
    \]
\end{frame}

\begin{frame}[t]
    \frametitle{One last step}

    Taking \(t(0)=1\) makes the recursion applicable for all $n \ge 0$.
    Together with \(t(1)=3\), 
    \[
        t(n)=
        c_{1} r_{1}^{n}
        +
        c_{2} r_{2}^{n}
    \]
    implies that
    \[
        c_{1}+c_{2} = 1, 
        \qquad
        c_{1} r_{1}
        +
        c_{2} r_{2}
        =3
    \]
    So we can fix $c_{1}$ and $c_{2}$ by solving these equations.
\end{frame}

\begin{frame}
    \frametitle{Example 9.12}
    
    Can we find all solutions to
    \begin{equation*}
        (A − 2)^2 f = 0.
    \end{equation*}

    \bomb{} The roots are not distinct!
\end{frame}

\begin{frame}[t]
    \frametitle{\tps{}}
    \begin{figure}
        \centering
        \includegraphics[width=0.7\textwidth]{2xn-grid.png}
    \end{figure}

    Let $t(n)$ be the number of ways to tile a $n \times 2$ chessboard by dominoes.
    Find a closed formula of $t(n)$.

    \hint{} For the convenience, let $t(0) = 1$. Also recall that
    \begin{equation*}
        x^2 + b x+c=\left(x+\frac{1}{2} \left(b-\sqrt{b^2-4 c}\right)\right)
        \left(x+\frac{1}{2} \left(b+\sqrt{b^2-4 c}\right)\right)
    \end{equation*}
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.9\textwidth]{exercise-14.jpg}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \courseassignment{}

            \href{https://www.rellek.net/book/app-comb.html}{Applied Combinatorics}
            \begin{itemize}
                \item[\emoji{pencil}] Section 9.9: 1, 3, 5, 7.
            \end{itemize}

            \hint{} For some problems, you can use WolframAlpha like 
            \href{https://www.wolframalpha.com/input/?i=SeriesCoefficient\%5Bx\%2F\%281-x\%29*\%281\%2Bx\%2Bx\%5E2\%2Bx\%5E3\%29\%281\%2F\%281-x\%5E4\%29\%29\%281\%2F\%281-x\%29\%29\%2C+\%7Bx\%2C+0\%2C+n\%7D\%5D}{this}.
        \end{column}
    \end{columns}
\end{frame}

\end{document}
