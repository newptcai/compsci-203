\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}
\input{../tikz.tex}

%\includeonlyframes{current}

\begin{document}

\maketitle

\begin{frame}[c]
    \frametitle{A few words before we start}
    
    Why should we learn a bit real analysis?
    \begin{itemize}
        \item[\laughing] Engineers use calculus, but pure mathematicians (and good engineers) use real analysis.
        \item[\emoji{persevere}] Analysis is the more rigorous version of calculus.
        \item[\cool{}] But we have only 4 lectures, so we will be less rigorous.
        \item[\emoji{triumph}] This should be taught in an advanced calculus course!
    \end{itemize}
\end{frame}

\lectureoutline{}

\section{\acs{bai} 1.2 The set of real numbers}

\begin{frame}[c]
    \frametitle{Ordered sets}
    
    \begin{block}{Definition 1.1.1}
        An \alert{ordered set} is a set $S$ together with a relation $<$ such that
        \begin{itemize}
            \item (i) (trichotomy) For all $x, y ∈ S$, exactly one of $x < y, x = y$,
                or $y < x$ holds.
            \item (ii) (transitivity) If $x, y, z ∈ S$ are such that $x < y$ and $y <
                z$, then $x < z$.
        \end{itemize}
    \end{block}

    \cake{} In the game of paper-scissors-stone, is
    $\{\temoji{hand}, \temoji{v}, \temoji{fist}\}$ an ordered set?
\end{frame}

\begin{frame}
    \frametitle{The maximum}

    \cake{} What are
    \begin{equation*}
        \max\{\text{Wealth of } \temoji{person-in-tuxedo} : \temoji{person-in-tuxedo}
        \in \temoji{earth-asia}\}
    \end{equation*}
    and
    \begin{equation*}
        \max\left\{
        1-\frac{1}{1},
        1-\frac{1}{2},
        1-\frac{1}{3},
        1-\frac{1}{4},
        1-\frac{1}{5}
        \right\}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Infinity is weird}
    
    What about
    \begin{equation*}
    \max\left\{1-\frac{1}{n}:n \in \dsN\right\}
    \end{equation*}

    \bomb{} In \acf{bai}, $\dsN = \{1, 2, 3, \dots\}$.

    \emoji{confounded} We need a notion of minimum and maximum for infinite sets.
\end{frame}

\begin{frame}
    \frametitle{Upper bound}

    Let $E ⊂ S$, where $S$ is an ordered set.

    \begin{block}{Definition 1.1.2}
        If there exists a $b \in S$ such that $x ≤ b$ for all $x ∈ E$, 
        then $E$ is \alert{bounded above} 
        and $b$ is an \alert{upper bound} of $E$.

        \cake{} What are some upper bounds of
        \begin{equation*}
            \left\{1-\frac{1}{n}:n \in \dsN\right\}
        \end{equation*}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Supremum}
    \begin{block}{Definition 1.1.2}
        If there exists an upper bound $b_0$ of $E$ such that whenever $b$ 
        is an upper bound for $E$ we have $b_0 ≤ b$, 
        then $b_0$ is called the \alert{supremum/least-upper-bound} of $E$ and we write
        \begin{equation*}
            \sup E \coloneqq b_0.
        \end{equation*}
    \end{block}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\textwidth]{figure-1-1.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Examples of supreme}
    
    \cake{} What are
    \begin{equation*}
        \sup\{\text{Wealth of } \temoji{person-in-tuxedo} : \temoji{person-in-tuxedo}
        \in \temoji{earth-asia}\}
    \end{equation*}
    and
    \begin{equation*}
        \sup\left\{
        1-\frac{1}{1},
        1-\frac{1}{2},
        1-\frac{1}{3},
        1-\frac{1}{4},
        1-\frac{1}{5}
        \right\}
    \end{equation*}
    \pause{}
    and
    \begin{equation*}
    \sup\left\{1-\frac{1}{n}:n \in \dsN\right\}
    \end{equation*}
\end{frame}
\begin{frame}
    \frametitle{Infimum}

    \begin{block}{Definition 1.1.2}
        If there exists a $b \in S$ such that $x \ge b$ for all $x ∈ E$, 
        then $E$ is \alert{bounded below} 
        and $b$ is an \alert{lower bound} of $E$.

        If there exists an upper bound $b_0$ of $E$ such that whenever $b$ 
        is an lower bound for $E$ we have $b_0 \ge b$, 
        then $b_0$ is called the \alert{infimum/greatest-lower-bound} of $E$ and we write
        \begin{equation*}
            \inf E \coloneqq b_0.
        \end{equation*}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{$\dsQ$ is not good enough}
    
    \begin{block}{Definition 1.1.3}
        An ordered set $S$ has the \alert{least-upper-bound} property 
        if every nonempty subset $E ⊂ S$ that is bounded above has a least upper
        bound, that is $\sup E$ exists in $S$.
    \end{block}
    
    \astonished{} Let $E =\{x ∈ \dsQ : x^2 < 2\}$, then
    \begin{equation*}
        \sup \{x ∈ \dsQ : x^2 < 2\} = \sqrt{2} \notin \dsQ.
    \end{equation*}
    Thus $\dsQ$ does not have \emph{least-upper-bound} property.
\end{frame}

\begin{frame}[c]
    \frametitle{What are real numbers?}
    
    \begin{block}{Theorem 1.2.1}
        There exists a unique \emph{ordered field} $\dsR$ with 
        the \alert{least-upper-bound} property such that $\dsQ ⊂ \dsR$.
    \end{block}

    \zany{} We don't know what is $\dsR$, but we know it is \emoji{+1}.
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    Let $D$ be the ordered set of all possible words using the English alphabet. 
    The order is the lexicographic order as in a dictionary (e.g.~ $a < an < dog <
    door$). 

    Let $A$ be the subset of $D$ containing the words whose first letter is `a'. 

    Show that $A$ has a supremum and find what it is.
\end{frame}

\section{\acs{bai} 2.1 Sequences and limits}

\begin{frame}[c]
    \frametitle{Limits}

    \begin{block}{Definition 2.1.1}
        A \alert{sequence (of real numbers)} is a function $x : \dsN \mapsto \dsR$. 

        The $n$th element in the sequence by $x_n$. 

        The notation $\{x_n\}$, or more precisely $\{x_n\}_{n=1}^{\infty}$ denotes a
        whole sequence.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Examples of sequences}

    Some sequences --
    \begin{equation*}
        \begin{aligned}
        \left\{\frac{1}{n}\right\}_{n=1}^{\infty} 
        &
        = 1, \frac{1}{2}, \frac{1}{3}, \frac{1}{4}, \frac{1}{5}\dots
        \\
        \left\{(-1)^{n}\right\}_{n=1}^{\infty} 
        &
        = 1, -1, 1, -1, \dots 
        \\
        \left\{\binom{2n}{n}\cdot\frac{1}{n+1}\right\}_{n=1}^{\infty} 
        &
        = 1, 2, 5, 14, 42, 132, 429, \dots
        \end{aligned}
    \end{equation*}

    \cake{} What's the name of the last one?
\end{frame}

\begin{frame}[c]
    \frametitle{Convergence in pictures}
    What does $\lim_{n \to \infty} x_{n} = x$ mean?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\textwidth]{convergence.png}
    \end{figure}
\end{frame}

\begin{frame}[c]
    \frametitle{Convergence}

    \begin{block}{Definition 2.1.2}
        A sequence $\{x_n\}$ \alert{converges} to $x ∈ \dsR$ if for every $ε > 0$, 
        there exists an $M ∈ \dsN$ such that $\abs{x_n − x} < ε$ for all $n ≥ M$. 

        The number $x$ is said to be the \emph{limit} of $\{x_n\}$.

        In this case, we write
        \begin{equation*}
            \lim_{n \to \infty} x_{n} = x
        \end{equation*}

        A sequence that converges is said to be \alert{convergent}. 
        Otherwise, it \alert{diverges}.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Example of convergence}

    \cake{} Are these sequences convergent? What are their limits?

    \begin{itemize}[<+->]
        \item $1, 1, 1, 1, \dots, $
        \item $\left\{\frac{1}{n}\right\}$
        \item $\{(-1)^{n}\}$
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Uniqueness of limits}
    
    \begin{block}{Proposition 2.1.6}
        A convergent sequence has a unique limit.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Boundedness of convergent sequence}
    
    \begin{block}{Proposition 2.1.7}
        A convergent sequence $\{x_n\}$ is bounded.
    \end{block}

    \cake{} The converse is not always true. Can you give an example?
\end{frame}

\begin{frame}
    \frametitle{Example 2.1.8}
    
    Is the sequence $\left\{\frac{n+1}{n+n^{2}}\right\}$ convergent?
    What is the limit?
\end{frame}

\begin{frame}
    \frametitle{What does our \emoji{robot} say?}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.9\textwidth]{bai-2-1-8.pdf}
    \end{figure}
\end{frame}

\subsection{2.1.1 Monotone sequences}

\begin{frame}[c]
    \frametitle{Monotone sequences}

    \begin{block}{Definition 2.1.9}
        A sequence $\{x_n\}$ is \alert{monotone increasing} 
        if $x_n ≤ x_n+1$ for all $n ∈ \dsN$. 

        A sequence $\{x_n\}$ is \alert{monotone decreasing} if \blankshort{}.
        (You can fill the gap here, right? \laughing{})

        If a sequence is either \emph{monotone increasing} or \emph{monotone
        decreasing}, we can simply say the sequence is \alert{monotone}.
    \end{block}

    \cake{} Give examples of sequences which are monotone \emoji{up-arrow}, 
    monotone \emoji{down-arrow}, and not-monotone.
\end{frame}

\begin{frame}
    \frametitle{Limits of monotone sequences --- Proposition 2.1.10}

    A monotone sequence ${x_n}$ is bounded if and only if it is convergent.

    Furthermore, if ${x_n}$ is monotone \emoji{up-arrow} and bounded, then
    \begin{equation*}
        \lim_{n \to \infty} x_n = \sup\{x_n : n ∈ \dsN\}.
    \end{equation*}

    \pause{}

    If ${x_n}$ is monotone \emoji{down-arrow} and bounded, then
    \begin{equation*}
        \lim_{n \to \infty} x_n = \inf\{x_n : n ∈ \dsN\}.
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Example 2.1.11}
    
    Is the sequence $\left\{\frac{1}{\sqrt{n}}\right\}$ convergent?
    What is the limit?
\end{frame}

\begin{frame}
    \frametitle{Example 2.1.12}
    
    Is the sequence $\left\{1 + \frac{1}{2} + \cdots + \frac{1}{n}\right\}$ convergent?
    What is the limit?
\end{frame}

\begin{frame}
    \frametitle{Proposition 2.1.13}

    Let $S ⊂ \dsR$ be a nonempty bounded set.

    Then there exist monotone sequences $\{x_n\}$ 
    and 
    $\{y_n\}$ such that $x_n, y_n \in S$ and
    \begin{equation*}
        \sup S = \lim_{n\to \infty} x_n
    \end{equation*}
    and
    \begin{equation*}
        \inf S = \lim_{n\to \infty} y_n
    \end{equation*}

    \cool{} Proof left as exercise.
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \exercisepic{\lecturenum}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \assignmentlastweek{}
            \begin{itemize}
                \item[\emoji{pencil}] Exercise 1.1.1-1.1.7
                \item[\emoji{pencil}] Exercise 2.1.1-2.1.13
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
