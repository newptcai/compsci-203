\input{../options.tex}
\documentclass{beamer}
\input{../meta.tex}
\input{../tikz.tex}

%\includeonlyframes{current}

\usepackage{csquotes}

\begin{document}

\maketitle

\section{\acs{bai} 2.5 Series}

\subsection{2.5.1 Definition}

\begin{frame}
    \frametitle{What is a series?}

    \begin{block}{Definition 2.5.1}
        Given a sequence $\{x_n\}$, we call
        $\sum_{n=1}^{\infty} x_{n}$
        a \alert{series}. 

        A series converges if the sequence $\{s_k\}$ defined by
        \begin{equation*}
        s_k := \sum_{n=1}^{k} x_n = x_{1} + \dots x_{k}
        \end{equation*}
        converges. In this case, we let
        \begin{equation*}
            \sum_{n=1}^{\infty} x_{n} = \lim_{k \to \infty} s_{k}.
        \end{equation*}

        The numbers $s_k$ are called \alert{partial sums}.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Example 2.5.4}
    In \emoji{sweden}, it is impolite to eat the last piece of \emoji{birthday}.

    If the first Swede eats $\frac{1}{2}$ of the \emoji{birthday},
    the second eats $\frac{1}{4}$ of the cake, and so on,
    will people be able to finish one \emoji{birthday}?
\end{frame}

\begin{frame}
    \frametitle{How many \emoji{birthday} do we need?}

    \begin{block}{Proposition 2.5.5}
        Suppose $−1 < r < 1$. Then the \alert{geometric series}
        $\sum_{n=0}^{\infty} r^{n}$
        converges, and
        \begin{equation*}
            \sum_{n=0}r^{n} = \frac{1}{1-r}
        \end{equation*}
    \end{block}

    \astonished{} Taking $r=1/2$. This implies that we need $2$ \emoji{birthday}
    to satisfy the \emoji{sweden} people instead of $1$ as on the previous slide.
    Why?
\end{frame}

\begin{frame}
    \frametitle{The begin does not matter}

    \begin{block}{Proposition 2.5.6}
        Let $M \in \dsN$.
        Then
        \begin{equation*}
            \sum_{n=1}^{\infty} x_{n}
            \text{ converges }
            \iff
            \sum_{n=M}^{\infty} x_{n}
            \text{ converges }
        \end{equation*}
    \end{block}
\end{frame}

\subsection{2.5.2 Cauchy series}

\begin{frame}
    \frametitle{Cauchy series}
    
    \begin{block}{Definition 2.5.7}
        A series $∑ x_n$ is said to be or \alert{a Cauchy series}
        if the sequence of partial sums $\{s_n\}$ is a Cauchy sequence.
    \end{block}

    \pause{}

    \begin{block}{Proposition 2.5.8}
        The series $∑ x_n$ is Cauchy 
        if for every $ε > 0$,
        there exists an $M ∈ N$ such that for every 
        $n ≥ M$ and every $k > n$, we have
        \begin{equation*}
            \sum_{j=n+1}^{k} x_{j} < \epsilon.
        \end{equation*}
    \end{block}
\end{frame}


\subsection{2.5.3 Basic properties}

\begin{frame}
    \frametitle{When a series converges}
    
    \begin{block}{Proposition 2.5.9}
    Let $∑ x_n$ be a convergent series. 
    Then the sequence $\{x_n\}$ is convergent and
    \begin{equation*}
        \lim_{n \to \infty} x_{n} =0
    \end{equation*}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{When does a geometric series diverge?}
    
    \begin{exampleblock}{Example 2.5.10}
        If $r ≥ 1$ or $r ≤ −1$, 
        then the geometric series $\sum_{n=1}^{\infty} r^{n}$ diverges.
    \end{exampleblock}
\end{frame}

\begin{frame}
    \frametitle{Harmonic numbers/series}
    
    \begin{exampleblock}{Example 2.5.11}
        \astonished{} The series $∑ 1/n$ diverges even though $1/n \to 0$.
    \end{exampleblock}
\end{frame}

\begin{frame}
    \frametitle{Linearity of series}

    \begin{block}{Proposition 2.5.12}
    Let $α ∈ \dsR$ and $∑ x_n$ and $∑ y_n$ be convergent series.
    Then

    (i) $∑ αx_n$ is a convergent series and
    \begin{equation*}
        \sum_{n=1}^{\infty} \alpha x_n = \alpha \sum_{n=1}^{\infty} x_{n}.
    \end{equation*}

    \pause{}

    (ii) $∑(x_n + y_n)$ is a convergent series and
    \begin{equation*}
        \sum_{n=1}^{\infty} (x_n + y_n) 
        = 
        \left(\sum_{n=1}^{\infty} x_n\right)
        +
        \left(\sum_{n=1}^{\infty} y_n\right)
        .
    \end{equation*}
    \end{block}
\end{frame}

\begin{frame}[c]
    \frametitle{\coffee{} --- Can a \emoji{robot} do these proofs?}
    
    \begin{columns}[c]
        \begin{column}{0.6\textwidth}
            Gowers \href{https://gowers.wordpress.com/2022/04/28/announcing-an-automatic-theorem-proving-project/}{wrote} ---

            \smallskip{}

            \begin{displayquote}
                Ever since Turing, 
                we have known that there is no \emoji{robot} 
                that will take as input a mathematical statement 
                and output a proof if the statement has a proof...

                Despite this, \emoji{teacher} regularly find long and \dizzy{}
                proofs of theorems. How is this possible?
            \end{displayquote}
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\textwidth]{Gowers.jpg}
                \caption{Sir William Timothy Gowers, British Mathematician}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\subsection{2.5.4 Absolute convergence}

\begin{frame}
    \frametitle{When $x_n \ge 0$}
    
    \begin{block}{Proposition 2.5.13}
        If $x_n ≥ 0$ for all $n$, 
        then $∑ x_n$ converges if 
        and only if the sequence of partial sums is bounded above.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Converge Absolutely}

    \begin{block}{Definition 2.5.14}
        A series $∑ x_n$ \alert{converges absolutely} if the series 
        $∑ |x_n|$ converges. 

        If a series converges, but does not converge absolutely, 
        we say it is \alert{conditionally convergent}.
    \end{block}

    \pause{}

    \begin{block}{Proposition 2.5.15}
    If the series $∑ x_n$ converges absolutely, then it converges.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Converge only conditionally}
    
    The series
    \begin{equation*}
        \sum_{n=1}^{\infty} \frac{(-1)^{n}}{n}
    \end{equation*}
    converges only conditionally.
\end{frame}

\subsection{2.5.5 Comparison test and the $p$-series}

\begin{frame}
    \frametitle{Comparison test}
    
    \begin{block}{Proposition 2.5.16}
        Let $∑ x_n$ and $∑ y_n$ be series such that $0 ≤ x_n ≤ y_n$ for all
        $n ∈ \dsN$.

        (i) If $∑ y_n$ converges, then so does $∑ x_n$.

        (ii) If $∑ x_n$ diverges, then so does $∑ y_n$.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{$p$-test}
    
    \begin{block}{Proposition 2.5.17}
        For $p ∈ \dsR$, the series
        \begin{equation*}
            \sum_{n=1}^{\infty} \frac{1}{n^{p}}
        \end{equation*}
        converges if and only if $p > 1$.
    \end{block}

    \pause{}

    \begin{block}{Example 2.5.18}
        The series $∑ \frac{1}{n^2+1}$ converges.
    \end{block}
\end{frame}

\subsection{2.5.6 Ratio test}

\begin{frame}
    \frametitle{Ratio test}
    
    \begin{block}{Proposition 2.5.19}
        Let $∑ x_n$ be a series, $x_n \ne 0$ for all $n$, and such that
        \begin{equation*}
            L
            \coloneq
            \lim_{n \to \infty} \frac{\abs{x_{n+1}}}{\abs{x_n}}
            \qquad
            \text{exists.}
        \end{equation*}

        (i) If $L < 1$, then $∑ x_n$ converges absolutely.

        (ii) If $L > 1$, then $∑ x_n$ diverges.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Try ratio test}
    
    \begin{exampleblock}{Example 2.5.20}
    The series
    \begin{equation*}
        \sum_{n=1}^{\infty} \frac{2^n}{n!}
    \end{equation*}
    converges absolutely.
    \end{exampleblock}
\end{frame}

\begin{frame}
    \frametitle{\tps{}}
    
    Which of the following converge? Why?

    \begin{enumerate}
        \item $\displaystyle \sum_{n=1}^{\infty} \frac{3}{9n+1}$
        \item $\displaystyle \sum_{n=1}^{\infty} \frac{1}{2n-1}$
        \item $\displaystyle \sum_{n=1}^{\infty} \frac{(-1)^n}{n^2}$
        \item $\displaystyle \sum_{n=1}^{\infty} \frac{1}{n(n+1)}$
        \item $\displaystyle \sum_{n=1}^{\infty} \frac{n}{e^{n^{2}}}$
    \end{enumerate}
\end{frame}

\appendix{}

\begin{frame}[c]
    \frametitle{Assignment}

    \begin{columns}[c]
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \exercisepic{\lecturenum}
            \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
            \assignmentlastweek{}
            \begin{itemize}
                \item[\emoji{pencil}] Exercise 2.5:1-4, 6, 8, 10-12.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
